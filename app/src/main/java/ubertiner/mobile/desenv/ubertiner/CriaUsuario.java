package ubertiner.mobile.desenv.ubertiner;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import ubertiner.mobile.desenv.ubertiner.FORMHELPER.FormularioHelperUsuario;
import ubertiner.mobile.desenv.ubertiner.MODELO.Usuario;

public class CriaUsuario extends AppCompatActivity {

    Intent in;
    UbertinerDAO dao = new UbertinerDAO(this);
    FormularioHelperUsuario helperUsuario;
    Usuario usuario;
    Spinner sp_cria_cidade;
    ArrayAdapter<String> arrayAdapterCidade;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cria_usuario);

        getNomesCidadesSpinner();


    }

    public void getNomesCidadesSpinner(){

        arrayAdapterCidade = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);
        arrayAdapterCidade.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        arrayAdapterCidade.addAll(dao.listarNomesCidades());
        sp_cria_cidade = (Spinner) findViewById(R.id.sp_cria_cidade);

        sp_cria_cidade.setAdapter(arrayAdapterCidade);
    }

    public void salvarUsuario(View view){
        helperUsuario = new FormularioHelperUsuario(this);
        usuario = helperUsuario.getUsuario(view);

        EditText tv_senha = (EditText) findViewById(R.id.edt_cria_senha);
        EditText tv_confirma_senha = (EditText) findViewById(R.id.edt_confirma_senha);

        if(tv_senha.getText().toString().compareTo(tv_confirma_senha.getText().toString()) == 0) {

            if (dao.cadastrarUSUARIO(usuario)) {
                in = new Intent(CriaUsuario.this, TelaLogin.class);
                startActivity(in);
            } else {
                Toast.makeText(this, "Houve um erro ao realizar operaçao", Toast.LENGTH_LONG).show();
            }
        }else{
            Toast.makeText(this, "CONFIRME A SENHA NOVAMENTE", Toast.LENGTH_LONG).show();
        }


    }



}
