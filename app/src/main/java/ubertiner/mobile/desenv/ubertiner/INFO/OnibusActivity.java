package ubertiner.mobile.desenv.ubertiner.INFO;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import ubertiner.mobile.desenv.ubertiner.MODELO.Horario;
import ubertiner.mobile.desenv.ubertiner.MODELO.Onibus;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;

public class OnibusActivity extends AppCompatActivity {
    ListView lv_detalhe_onibus;
    ListView lv_detalhe_onibus2;
    private int adapterLayout = android.R.layout.simple_list_item_1;
    private ArrayAdapter<Horario> adapterOnibus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onibus);

        lv_detalhe_onibus = (ListView) findViewById(R.id.lv_detalhe_onibus);
        lv_detalhe_onibus2 = (ListView) findViewById(R.id.lv_detalhe_onibus);

        lv_detalhe_onibus2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(OnibusActivity.this, DetalheHorarioOnibusActivity.class);
                Horario horarioSelecionado = (Horario) lv_detalhe_onibus2.getItemAtPosition(position);
                it.putExtra("Horario_Selecionado", horarioSelecionado);
                startActivity(it);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        String nomeOnibus = intent.getStringExtra("Onibus_Selecionado");
        UbertinerDAO dao = new UbertinerDAO(this);
        List<Horario> horario = dao.buscaOnibus(nomeOnibus);
        dao.close();
        if(horario == null){
            Toast.makeText(this, "Nao ha dados cadastrados para " + nomeOnibus, Toast.LENGTH_LONG).show();
        }else {
            this.adapterOnibus = new ArrayAdapter<Horario>(this, adapterLayout, horario);
            this.lv_detalhe_onibus.setAdapter(adapterOnibus);
        }
    }
}
