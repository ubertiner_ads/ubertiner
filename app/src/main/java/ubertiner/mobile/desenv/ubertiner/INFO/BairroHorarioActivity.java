package ubertiner.mobile.desenv.ubertiner.INFO;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.MAPAS.MapsActivity;
import ubertiner.mobile.desenv.ubertiner.MODELO.Bairro;
import ubertiner.mobile.desenv.ubertiner.MODELO.Horario;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.TelaPrincipal;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;

public class BairroHorarioActivity extends AppCompatActivity {
    Horario horarioOnibus = new Horario();
    List<String> horas = new ArrayList<>();
    UbertinerDAO dao;
    String resultado = "";

    TextView tv_nome_tempo;
    TextView tv_nome_rua;
    TextView tv_nome_bairro;
    TextView tv_nome_onibus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bairro_horario);

        tv_nome_onibus = (TextView) findViewById(R.id.tv_nome_onibuss2);
        tv_nome_bairro = (TextView) findViewById(R.id.tv_nome_bairros2);
        tv_nome_rua = (TextView) findViewById(R.id.tv_nome_ruas2);
        tv_nome_tempo = (TextView) findViewById(R.id.tv_tempos2);

        String tempo;
        String onibus;
        Horario horario = (Horario) getIntent().getSerializableExtra("Horario_Selecionado");
        tempo = horario.getTempo_1();
        onibus = horario.getNomeOnibus();
        UbertinerDAO dao  = new UbertinerDAO(this);
        horarioOnibus = dao.buscaOnibusHorario2(tempo, onibus);

        Toast.makeText(this, "Relaçao de horarios: " + horarioOnibus, Toast.LENGTH_LONG).show();System.out.println("MINHA HORA A SER BUSCADA E: " +horarioOnibus.getTempo_1());
        horas.add(horarioOnibus.getTempo_1());

        //ESSE BLOCO PEGA A DATA E HORA ATUAL JÁ NO FORMATO PADRAO
        GregorianCalendar calendar = new GregorianCalendar();
        SimpleDateFormat formatador = new SimpleDateFormat("dd'/'MM'/'yyyy' 'HH':'mm");
        String stringHoraAtual = formatador.format(calendar.getTime());

        //INICIALIZO AS VARIAVEIS QUE VOU USAR NO BLOCO TRY/CATCH
        Date dataHoraAtual = null;
        List<Date> dataHoraBanco = new ArrayList<>();
        String stringDataTratada = "";


        /*  ... CONTINUA  */
        for (int i = 0; i < horas.size(); i++) {
            try {
                //AQUI EU TRATO A MINHA STRING...
                dataHoraAtual = formatador.parse(stringHoraAtual);


                //... INSERINDO A DATA ATUAL CONCATENADA...
                stringDataTratada = stringHoraAtual.substring(0, 11) + horas.get(i);
                System.out.println("ESTOU ADICIONANDO ISSO NO MEU ARRAY//////////////" + stringDataTratada);

                //... PORQUE EU VOU PRECISAR DELA PARA FAZER A MANIPULAÇÃO DA DATA/HORA
                try {
                    dataHoraBanco.add(formatador.parse(stringDataTratada));

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }



        int cont = 0;
        while (dataHoraAtual.getTime() > dataHoraBanco.get(cont).getTime()) {
            //AQUI EU PEGO A DIFERENÇA EM MILISSEGUNDOS
            cont++;
        }
        System.out.println("/////////////////////////// VALOR QUE SAI" + dataHoraBanco.get(cont).toString());
        System.out.println("/////////////////////////// MINHA HORA ATUAL" + dataHoraAtual.getTime());

        long diferencaLong = dataHoraBanco.get(cont).getTime() - dataHoraAtual.getTime();
        System.out.print(diferencaLong + " DIFERENCA LONG///////////////////// ");

        if (diferencaLong != 0) {

            long minutos = diferencaLong / 60000;
//            long horas = diferencaLong / (60 * 60 * 1000) % 24;
            if (minutos > 59) {
//            System.out.println(horas + " horas////////////////// ");
                System.out.println("falta mais de uma hora/////////////////// ");
                resultado=("Falta mais de uma hora...");
            } else {
                System.out.println(minutos + " minutos///////////////////// ");
                resultado=("Faltam "+ minutos + "minutos pra passar");
            }
        } else {
            System.out.println("BUSAO TA PASSANDO AGORA CARAI///////////////////// ");
            diferencaLong = dataHoraBanco.get(cont + 1).getTime() - dataHoraAtual.getTime();
            if (diferencaLong != 0) {

                long proximo = diferencaLong / 60000;
                //            long horas = diferencaLong / (60 * 60 * 1000) % 24;
                if (proximo > 59) {
                    //            System.out.println(horas + " horas////////////////// ");
                    System.out.println("falta mais de uma hora/////////////////// ");
                    resultado=("Falta mais de uma hora...");
                } else {
                    System.out.println(proximo + " minutos///////////////////// ");
                    resultado=("Está passando em poucos instantes");
                }
                System.out.println("O PROXIMO PASSA EM :  " +proximo+"  MINUTOS");
            }
        }
        /* METODO QUE SETA VALORES NOS TEXTVIEW COM INFORMAÇÕES DOS ONIBUS*/
        povoaCampos();

    }

    public void voltarMenu1(View view){
        Intent in = new Intent(BairroHorarioActivity.this, TelaPrincipal.class);
        startActivity(in);
    }

    public void povoaCampos(){

        System.out.println("++++++/////////// NOME ONIBUS" + horarioOnibus.getNomeOnibus() + "  -NOME BAIRRO  " + horarioOnibus.getNomeBairro());
        System.out.println("++++++ NOME RUA " + horarioOnibus.getNomeRua() + " +MEU RESULTADO " + resultado + ");");
        tv_nome_onibus.setText("" + horarioOnibus.getNomeOnibus());
        tv_nome_bairro.setText("" + horarioOnibus.getNomeBairro());
        tv_nome_rua.setText(""+horarioOnibus.getNomeRua());
        tv_nome_tempo.setText("" + resultado);

    }

    public void visualizarNoMapa(View view){
        dao = new UbertinerDAO(this);
        float[] coordenadas = new float[2];

        coordenadas[0] = horarioOnibus.getLatitude();
        coordenadas[1] = horarioOnibus.getLongitude();

        Intent in = new Intent(BairroHorarioActivity.this, MapsActivity.class);
        Toast.makeText(this, "LAT " + coordenadas[0] + "   LONG   "+  coordenadas[1] +"  ", Toast.LENGTH_LONG).show();
        in.putExtra("latitude", coordenadas[0] + "");  //STRING
        in.putExtra("longitude", coordenadas[1] + "");  //STRING
        startActivity(in);
    }
}
