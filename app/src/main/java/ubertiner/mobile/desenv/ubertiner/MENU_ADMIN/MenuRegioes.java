package ubertiner.mobile.desenv.ubertiner.MENU_ADMIN;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import ubertiner.mobile.desenv.ubertiner.CRUDS.AlterarRegioes;
import ubertiner.mobile.desenv.ubertiner.CRUDS.ExcluirRegioes;
import ubertiner.mobile.desenv.ubertiner.CRUDS.InserirRegioes;
import ubertiner.mobile.desenv.ubertiner.MenuAdmin;
import ubertiner.mobile.desenv.ubertiner.R;

public class MenuRegioes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_regioes);
    }

    public void inserir(View view){
        Intent in = new Intent(MenuRegioes.this, InserirRegioes.class);
        startActivity(in);
    }

    public void alterar(View view){
        Intent in = new Intent(MenuRegioes.this, AlterarRegioes.class);
        startActivity(in);
    }

    public void excluir(View view){
        Intent in = new Intent(MenuRegioes.this, ExcluirRegioes.class);
        startActivity(in);
    }

    public void voltar(View view){
        Intent in = new Intent(MenuRegioes.this, MenuAdmin.class);
        startActivity(in);
    }
}
