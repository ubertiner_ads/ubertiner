package ubertiner.mobile.desenv.ubertiner;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.Fragments.BairrosFragment;
import ubertiner.mobile.desenv.ubertiner.Fragments.OnibusFragment;
import ubertiner.mobile.desenv.ubertiner.Fragments.RuasFragment;
import ubertiner.mobile.desenv.ubertiner.MODELO.Usuario;

public class TelaLogin extends AppCompatActivity {

    UbertinerDAO dao = new UbertinerDAO(this);
    Intent in;
    private ViewPager mViewPager;

    List<String> horarios = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_login);
//
//        horarios = dao.mostraHorarios();
//
//        Toast.makeText(this, "horarios:   " + horarios, Toast.LENGTH_LONG).show();

    }



    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

                case 0:
                    return RuasFragment.newInstance("RuasFragment, Instance 1");
                case 1:
                    return BairrosFragment.newInstance("BairrosFragment, Instance 1");
                case 2:
                    return OnibusFragment.newInstance("OnibusFragment, Instance 1");
                case 3:
                    return OnibusFragment.newInstance("OnibusFragment, Instance 2");
                case 4:
                    return OnibusFragment.newInstance("OnibusFragment, Instance 3");
                default:
                    return OnibusFragment.newInstance("OnibusFragment, Default");
            }
        }
        @Override
        public int getCount() {
            return 3;
        }
    }

    public void criaUsuario(View view){
        Intent in = new Intent(TelaLogin.this, CriaUsuario.class);
        startActivity(in);
    }

    public void validaLogin(View view){

        EditText edt_usuario = (EditText) findViewById(R.id.edt_usuario);
        EditText edt_senha = (EditText) findViewById(R.id.edt_senha);
        String nome_usuario = edt_usuario.getText().toString();
        String senha = edt_senha.getText().toString();

        try {
            if (dao.validarAcesso(nome_usuario, senha)) {
                in = new Intent(TelaLogin.this, TelaPrincipal.class );

                startActivity(in);
            } else {
                Toast.makeText(this, "usuario invalido", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            Toast.makeText(this, "Houve um ERRO!   " +e, Toast.LENGTH_LONG).show();
        }
    }

    public void loginAdmin(View view){
        Intent in = new Intent(TelaLogin.this, LoginAdmin.class);
        startActivity(in);
    }

}
