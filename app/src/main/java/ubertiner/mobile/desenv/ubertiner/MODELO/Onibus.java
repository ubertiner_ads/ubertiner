package ubertiner.mobile.desenv.ubertiner.MODELO;

import java.io.Serializable;

/**
 * Created by SamuelMiranda on 01/05/2016.
 */
public class Onibus implements Serializable {

    private int codigo;
    private String nomeOnibus;
    private String empresa;

    public Onibus() {
    }

    public Onibus(int codigo, String nomeOnibus, String empresa) {
        this.codigo = codigo;
        this.nomeOnibus = nomeOnibus;
        this.empresa = empresa;
    }

    @Override
    public String toString() {
        return  "Nome Onibus: " + nomeOnibus;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNomeOnibus() {
        return nomeOnibus;
    }

    public void setNomeOnibus(String nomeOnibus) {
        this.nomeOnibus = nomeOnibus;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
}
