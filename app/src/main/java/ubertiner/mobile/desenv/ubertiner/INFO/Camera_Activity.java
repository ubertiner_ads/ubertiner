package ubertiner.mobile.desenv.ubertiner.INFO;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import ubertiner.mobile.desenv.ubertiner.R;


public class Camera_Activity extends Activity implements QRCodeReaderView.OnQRCodeReadListener {

    private QRCodeReaderView mydecoderview;
    private ImageView line_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_);

        Intent i = getIntent();

        mydecoderview = (QRCodeReaderView) findViewById(R.id.qrcoderview);
        mydecoderview.setOnQRCodeReadListener(this);

        //faz a linha vermelha piscar
        line_image = (ImageView) findViewById(R.id.red_line);

        TranslateAnimation mAnimation = new TranslateAnimation(
                    TranslateAnimation.ABSOLUTE, 0f,
                    TranslateAnimation.ABSOLUTE, 0f,
                    TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                    TranslateAnimation.RELATIVE_TO_PARENT, 0.5f);

        mAnimation.setDuration(1000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        line_image.setAnimation(mAnimation);
    }

    //é chamado quando um QRCode é encontrado
    //coloca o texto no text view
    @Override
    public void onQRCodeRead(String nome_rua, PointF[] points) {
        //myTextView.setText(text);
        Intent i = new Intent(Camera_Activity.this, OnibusRuaActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("Rua_Selecionada", nome_rua);
        System.out.println("Nome da Rua recebida do QR CODE:  " + nome_rua);
        i.putExtras(bundle);
        startActivity(i);

    }

    // é chamado caso seu dispositivo não tenha câmera
    @Override
    public void cameraNotFound() {

    }

    //é chamado quando não tem QRCode na Imagem
    @Override
    public void QRCodeNotFoundOnCamImage() {

    }

    @Override
    public void onResume() {
        super.onResume();
        mydecoderview.getCameraManager().startPreview();
    }

    public void onPause(){
        super.onPause();
        mydecoderview.getCameraManager().stopPreview();
    }
}
