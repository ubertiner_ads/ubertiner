package ubertiner.mobile.desenv.ubertiner.MENU_ADMIN;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import ubertiner.mobile.desenv.ubertiner.CRUDS.AlterarRuas;
import ubertiner.mobile.desenv.ubertiner.CRUDS.ExcluirRuas;
import ubertiner.mobile.desenv.ubertiner.CRUDS.InserirRuas;
import ubertiner.mobile.desenv.ubertiner.MenuAdmin;
import ubertiner.mobile.desenv.ubertiner.R;

public class MenuRuas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_ruas);
    }

    public void inserir(View view){
        Intent in = new Intent(MenuRuas.this, InserirRuas.class);
        startActivity(in);
    }

    public void alterar(View view){
        Intent in = new Intent(MenuRuas.this, AlterarRuas.class);
        startActivity(in);
    }

    public void excluir(View view){
        Intent in = new Intent(MenuRuas.this, ExcluirRuas.class);
        startActivity(in);
    }

    public void voltar(View view){
        Intent in = new Intent(MenuRuas.this, MenuAdmin.class);
        startActivity(in);
    }
}
