package ubertiner.mobile.desenv.ubertiner.MODELO;

import java.io.Serializable;

/**
 * Created by SamuelMiranda on 01/05/2016.
 */
public class Cidade implements Serializable{
    private String nomeCidade;
    private String estado;

    public Cidade() {
    }

    public Cidade(String nomeCidade, String estado) {

        this.nomeCidade = nomeCidade;
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Cidade{" +
                "nomeCidade='" + nomeCidade + '\'' +
                ", estado='" + estado + '\'' +
                '}';
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
