package ubertiner.mobile.desenv.ubertiner.MODELO;

import java.io.Serializable;

/**
 * Created by SamuelMiranda on 01/05/2016.
 */
public class Bairro implements Serializable{

    private String nomeBairro;
    private String nomeRegiao;

    public Bairro() {
    }

    public Bairro(String nomeBairro, String nomeRegiao) {
        this.nomeBairro = nomeBairro;
        this.nomeRegiao = nomeRegiao;
    }

    @Override
    public String toString() {
        return "Bairro{" +
                "nomeBairro='" + nomeBairro + '\'' +
                ", nomeRegiao='" + nomeRegiao + '\'' +
                '}';
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }

    public String getNomeRegiao() {
        return nomeRegiao;
    }

    public void setNomeRegiao(String nomeRegiao) {
        this.nomeRegiao = nomeRegiao;
    }
}
