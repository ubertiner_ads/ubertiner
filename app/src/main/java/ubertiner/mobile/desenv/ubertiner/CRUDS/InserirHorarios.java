package ubertiner.mobile.desenv.ubertiner.CRUDS;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import ubertiner.mobile.desenv.ubertiner.FORMHELPER.FormularioHelperHorario;
import ubertiner.mobile.desenv.ubertiner.MENU_ADMIN.MenuHorarios;
import ubertiner.mobile.desenv.ubertiner.MODELO.Horario;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;

public class InserirHorarios extends AppCompatActivity {

    Intent in;
    UbertinerDAO dao = new UbertinerDAO(this);
    FormularioHelperHorario helperHorario;
    Horario horario;
    Spinner sp_cidade;
    Spinner sp_regiao;
    Spinner sp_bairro;
    Spinner sp_rua;
    Spinner sp_onibus;
    ArrayAdapter<String> arrayAdapterCidade;
    ArrayAdapter<String> arrayAdapterRegiao;
    ArrayAdapter<String> arrayAdapterBairro;
    ArrayAdapter<String> arrayAdapterRua;
    ArrayAdapter<String> arrayAdapterOnibus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inserir_horarios);

        getNomesCidadesSpinner();
        getNomesRegioesSpinner();
        getNomesBairrosSpinner();
        getNomesRuasSpinner();
        getNomesOnibusSpinner();

    }


    public void getNomesCidadesSpinner(){

        arrayAdapterCidade = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);
        arrayAdapterCidade.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        arrayAdapterCidade.addAll(dao.listarNomesCidades());
        sp_cidade = (Spinner) findViewById(R.id.sp_cidade_v);

        sp_cidade.setAdapter(arrayAdapterCidade);
    }

    public void getNomesBairrosSpinner(){

        arrayAdapterBairro= new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);
        arrayAdapterBairro.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        arrayAdapterBairro.addAll(dao.listarNomesBairros());
        sp_bairro = (Spinner) findViewById(R.id.sp_bairro_v);

        sp_bairro.setAdapter(arrayAdapterBairro);
    }

    public void getNomesRegioesSpinner(){

        arrayAdapterRegiao = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);
        arrayAdapterRegiao.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        arrayAdapterRegiao.addAll(dao.listarNomesRegioes());
        sp_regiao = (Spinner) findViewById(R.id.sp_regiao_v);

        sp_regiao.setAdapter(arrayAdapterRegiao);
    }

    public void getNomesRuasSpinner(){

        arrayAdapterRua = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);
        arrayAdapterRua.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        arrayAdapterRua.addAll(dao.listarRuas());
        sp_rua = (Spinner) findViewById(R.id.sp_rua_v);

        sp_rua.setAdapter(arrayAdapterRua);
    }

    public void getNomesOnibusSpinner(){

        arrayAdapterOnibus = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);
        arrayAdapterOnibus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        arrayAdapterOnibus.addAll(dao.listarNomesOnibus());
        sp_onibus = (Spinner) findViewById(R.id.sp_onibus_v);

        sp_onibus.setAdapter(arrayAdapterOnibus);
    }

    public void salvarHorario(View view){
        helperHorario = new FormularioHelperHorario(this);
        horario = helperHorario.getHorario(view);

            if (dao.cadastrarHORARIO(horario)) {
                Toast.makeText(this, "NOVO HORARIO CADASTRADO COM SUCESSO", Toast.LENGTH_LONG).show();
                in = new Intent(InserirHorarios.this, MenuHorarios.class);
                startActivity(in);
            } else {
                Toast.makeText(this, "Houve um erro ao realizar operaçao", Toast.LENGTH_LONG).show();
            }

    }

}
