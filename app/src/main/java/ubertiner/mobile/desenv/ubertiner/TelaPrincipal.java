package ubertiner.mobile.desenv.ubertiner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


import ubertiner.mobile.desenv.ubertiner.Fragments.HorariosFragment;
import ubertiner.mobile.desenv.ubertiner.Fragments.OnibusFragment;
import ubertiner.mobile.desenv.ubertiner.Fragments.QRCodeFragment;
import ubertiner.mobile.desenv.ubertiner.Fragments.RuasFragment;
import ubertiner.mobile.desenv.ubertiner.Fragments.BairrosFragment;
import ubertiner.mobile.desenv.ubertiner.MODELO.Usuario;
import ubertiner.mobile.desenv.ubertiner.extras.SlidingTabLayout;

public class TelaPrincipal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private Toolbar mToolbar;
    private Toolbar mToolbarBottom;
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private TextView tv_nome_drawer;

    UbertinerDAO dao = new UbertinerDAO(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mToolbar.setTitle("UBERTINER");
        mToolbar.setLogo(R.drawable.mini_logo);
        setSupportActionBar(mToolbar);

        mToolbarBottom = (Toolbar) findViewById(R.id.include_toolbar_bottom);
        mToolbarBottom.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){

                    case R.id.action_facebook:
                        Toast.makeText(TelaPrincipal.this, "INTERAÇAO REDE SOCIAL FACEBOOK", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.action_youtube:
                        Toast.makeText(TelaPrincipal.this, "INTERAÇAO REDE SOCIAL YOUTUBE", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.action_whats:
                        Toast.makeText(TelaPrincipal.this, "INTERAÇAO REDE SOCIAL WHATSAPP", Toast.LENGTH_LONG).show();
                        break;
                }


                return true;
            }
        });
        mToolbarBottom.inflateMenu(R.menu.menu_bottom);


// -----------------VIEW PAGER

        mViewPager = (ViewPager) findViewById(R.id.viewpage_tabs);
        mViewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.colorAccent));
        mSlidingTabLayout.setCustomTabView(R.layout.tab_view, R.id.tv_tab);
        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mSlidingTabLayout.setViewPager(mViewPager);



//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

//  ----------------------- DRAWER NAVIGATION-------

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {

        private String[] titles = {"UBERPOINT", "RUAS", "BAIRROS", "ÔNIBUS", "HORARIOS"};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

                case 0:
                    return QRCodeFragment.newInstance("QRCodeFragment, Instance 1");
                case 1:
                    return RuasFragment.newInstance("RuasFragment, Instance 1");
                case 2:
                    return BairrosFragment.newInstance("BairrosFragment, Instance 1");
                case 3:
                    return OnibusFragment.newInstance("OnibusFragment, Instance 1");
                case 4:
                    return HorariosFragment.newInstance("HorariosFragment, Instance 2");
                default:
                    return OnibusFragment.newInstance("OnibusFragment, Default");
            }
        }
        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
    //        /*Drawable d = mContext.getResources().getDrawable( icons[position] );
    //        d.setBounds(0, 0, heightIcon, heightIcon);
    //
    //        ImageSpan is = new ImageSpan( d );
    //
    //        SpannableString sp = new SpannableString(" ");
    //        sp.setSpan( is, 0, sp.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
    //
    //
    //        return ( sp );*/
            return ( titles[position] );
        }

    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu_mapas; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tela_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void liberaSearch(View view){
        //função do botao search no TOOLBAR SUPERIOR DA TELA
    }



}
