package ubertiner.mobile.desenv.ubertiner.INFO;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.MODELO.Horario;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.TelaPrincipal;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;

public class OnibusHorarioActivity extends AppCompatActivity {

    ListView lv_detalhe_horarios;
    ListView lv_detalhe_horarios2;
    private int adapterLayout = android.R.layout.simple_list_item_1;

    //    Intent it = getIntent();
    private ArrayAdapter<Horario> adapterHorario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_horario);

        lv_detalhe_horarios = (ListView) findViewById(R.id.lv_detalhe_horario);
        lv_detalhe_horarios2 = (ListView) findViewById(R.id.lv_detalhe_horario);

        lv_detalhe_horarios2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(OnibusHorarioActivity.this, HorarioHorarioActivity.class);
                Horario horarioSelecionado = (Horario) lv_detalhe_horarios2.getItemAtPosition(position);
                it.putExtra("Horario_Selecionado", horarioSelecionado);
                startActivity(it);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String tempo;
        String onibus;
        Horario horario = (Horario) getIntent().getSerializableExtra("Horario_Selecionado");
        tempo = horario.getTempo_1();
        onibus = horario.getNomeOnibus();
        UbertinerDAO dao  = new UbertinerDAO(this);
        List<Horario> horarioOnibus = dao.buscaOnibusHorario(tempo, onibus);
        dao.close();
        if(horarioOnibus == null){
            Toast.makeText(this, "Nao ha dados cadastrados para " + horario.getNomeOnibus(), Toast.LENGTH_LONG).show();
            Intent in = new Intent(OnibusHorarioActivity.this, TelaPrincipal.class);
            startActivity(in);
        }else {
            this.adapterHorario = new ArrayAdapter<Horario>(this, adapterLayout, horarioOnibus);
            this.lv_detalhe_horarios.setAdapter(adapterHorario);
        }
    }
}
