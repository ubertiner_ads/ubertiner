package ubertiner.mobile.desenv.ubertiner.FORMHELPER;

import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import ubertiner.mobile.desenv.ubertiner.CRUDS.InserirHorarios;
import ubertiner.mobile.desenv.ubertiner.MODELO.Horario;
import ubertiner.mobile.desenv.ubertiner.R;

/**
 * Created by SamuelMiranda on 01/05/2016.
 */
public class FormularioHelperHorario {

    private Spinner nomeOnibus;
    private Spinner nomeCidade;
    private Spinner nomeRegiao;
    private Spinner nomeBairro;
    private Spinner nomeRua;

    private EditText hora;
    private EditText minuto;
    private String tempo_1;

    Horario horario;

    public FormularioHelperHorario(InserirHorarios activity){
        nomeCidade = (Spinner) activity.findViewById(R.id.sp_cidade_v);
        nomeRegiao = (Spinner) activity.findViewById(R.id.sp_regiao_v);
        nomeBairro = (Spinner) activity.findViewById(R.id.sp_bairro_v);
        nomeRua    = (Spinner) activity.findViewById(R.id.sp_rua_v);
        nomeOnibus = (Spinner) activity.findViewById(R.id.sp_onibus_v);

        hora = (EditText) activity.findViewById(R.id.horas);
        minuto = (EditText) activity.findViewById(R.id.minutos);
        tempo_1 = hora.getText().toString() + ":" + minuto.getText().toString();

        horario = new Horario();
    }

    public Horario getHorario(View view){

        horario.setNomeCidade(nomeCidade.getSelectedItem().toString());
        System.out.printf("///////////////////////////"+nomeCidade.getSelectedItem().toString());
        horario.setNomeRegiao(nomeRegiao.getSelectedItem().toString());
        horario.setNomeBairro(nomeBairro.getSelectedItem().toString());
        horario.setNomeRua(nomeRua.getSelectedItem().toString());
        horario.setNomeOnibus(nomeOnibus.getSelectedItem().toString());
        horario.setTempo_1(tempo_1);

        return horario;

    }


    public void setUsuario(){
//        IMPLEMENTAR CASO SEJA NECESSÁRIO
    }
}

