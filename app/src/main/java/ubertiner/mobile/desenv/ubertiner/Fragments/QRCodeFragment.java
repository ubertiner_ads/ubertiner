package ubertiner.mobile.desenv.ubertiner.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import java.util.ArrayList;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.INFO.Camera_Activity;
import ubertiner.mobile.desenv.ubertiner.INFO.OnibusBairroActivity;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;

/**
 * Created by SamuelMiranda on 23/10/2016.
 */
public class QRCodeFragment extends Fragment{/*VARIAVEIS INSTANCIADAS - IMPORTANTES*/

    Button button_qrcode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_qrcodefragment, container, false);

        button_qrcode = (Button) v.findViewById(R.id.bt_qrcode);

        button_qrcode.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                // Here, thisActivity is the current activity
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.CAMERA)) {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.CAMERA},0);
//                        Intent in = new Intent(getActivity().getBaseContext(), Camera_Activity.class);
//                        startActivity(in);

                    } else {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.CAMERA},0);
//                        Intent in = new Intent(getActivity().getBaseContext(), Camera_Activity.class);
//                        startActivity(in);

                    }
                }else{
                    Intent in = new Intent(getActivity().getBaseContext(), Camera_Activity.class);
                    startActivity(in);
                }
            }
        });

        //Retorno a View para o meu PagerView, ela é reconhecida através da interface do android
        return v;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            Intent in = new Intent(getActivity().getBaseContext(), Camera_Activity.class);
            startActivity(in);
        }

    }

    public static QRCodeFragment newInstance(String text) {

        QRCodeFragment frag = new QRCodeFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        frag.setArguments(b);

        return frag;
    }
}
