package ubertiner.mobile.desenv.ubertiner.MODELO;

import java.io.Serializable;

/**
 * Created by SamuelMiranda on 01/05/2016.
 */
public class Usuario implements Serializable {
    private String nome;
    private String senha;
    private String nomeCidade;
    private boolean tutorialInicial;

    public Usuario() {
    }

    public Usuario(String nome, String senha, String nomeCidade) {

        this.nome = nome;
        this.senha = senha;
        this.nomeCidade = nomeCidade;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nome='" + nome + '\'' +
                ", senha='" + senha + '\'' +
                ", nomeCidade='" + nomeCidade + '\'' +
                '}';
    }

    public boolean isTutorialInicial() {
        return tutorialInicial;
    }

    public void setTutorialInicial(boolean tutorialInicial) {
        this.tutorialInicial = tutorialInicial;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }
}
