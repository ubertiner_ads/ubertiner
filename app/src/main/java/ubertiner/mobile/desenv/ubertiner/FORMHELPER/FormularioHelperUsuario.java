package ubertiner.mobile.desenv.ubertiner.FORMHELPER;

import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import ubertiner.mobile.desenv.ubertiner.CriaUsuario;
import ubertiner.mobile.desenv.ubertiner.MODELO.Usuario;
import ubertiner.mobile.desenv.ubertiner.R;

/**
 * Created by SamuelMiranda on 01/05/2016.
 */
public class FormularioHelperUsuario {

    EditText nome;
    EditText senha;
    Spinner nomeCidade;

    Usuario usuario;

    public FormularioHelperUsuario(CriaUsuario activity){
        nome = (EditText) activity.findViewById(R.id.edt_cria_usuario);
        senha = (EditText) activity.findViewById(R.id.edt_cria_senha);
        nomeCidade = (Spinner) activity.findViewById(R.id.sp_cria_cidade);
        usuario = new Usuario();
    }

    public Usuario getUsuario(View view){
        usuario.setNome(nome.getText().toString());
        usuario.setSenha(senha.getText().toString());
        usuario.setNomeCidade(nomeCidade.getSelectedItem().toString());
//        usuario.setNomeCidade("Uberlandia");
        return usuario;
    }


    public void setUsuario(){
//        IMPLEMENTAR CASO SEJA NECESSÁRIO
    }
}
