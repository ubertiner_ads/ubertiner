package ubertiner.mobile.desenv.ubertiner.MENU_ADMIN;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import ubertiner.mobile.desenv.ubertiner.CRUDS.AlterarOnibus;
import ubertiner.mobile.desenv.ubertiner.CRUDS.ExcluirOnibus;
import ubertiner.mobile.desenv.ubertiner.CRUDS.InserirOnibus;
import ubertiner.mobile.desenv.ubertiner.MenuAdmin;
import ubertiner.mobile.desenv.ubertiner.R;

public class MenuOnibus extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_onibus);
    }

    public void inserir(View view){
        Intent in = new Intent(MenuOnibus.this, InserirOnibus.class);
        startActivity(in);
    }

    public void alterar(View view){
        Intent in = new Intent(MenuOnibus.this, AlterarOnibus.class);
        startActivity(in);
    }

    public void excluir(View view){
        Intent in = new Intent(MenuOnibus.this, ExcluirOnibus.class);
        startActivity(in);
    }

    public void voltar(View view){
        Intent in = new Intent(MenuOnibus.this, MenuAdmin.class);
        startActivity(in);
    }

}
