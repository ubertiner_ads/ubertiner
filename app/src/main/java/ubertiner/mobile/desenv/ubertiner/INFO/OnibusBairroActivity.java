package ubertiner.mobile.desenv.ubertiner.INFO;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.MODELO.Bairro;
import ubertiner.mobile.desenv.ubertiner.MODELO.Horario;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.TelaPrincipal;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;

public class OnibusBairroActivity extends FragmentActivity {

//    Intent it = getIntent();
    List<Horario> horariosOnibus = new ArrayList<>();
    private ArrayAdapter<Horario> adapterBairro;
    ListView lv_detalhebairros;
    ListView lv_detalhebairros2;
    private int adapterLayout = android.R.layout.simple_list_item_1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_bairro);

        lv_detalhebairros = (ListView) findViewById(R.id.lv_detalhe_bairro);
        lv_detalhebairros2 = (ListView) findViewById(R.id.lv_detalhe_bairro);

        lv_detalhebairros2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(OnibusBairroActivity.this, BairroHorarioActivity.class);
                Horario horarioSelecionado = (Horario) lv_detalhebairros2.getItemAtPosition(position);
                it.putExtra("Horario_Selecionado", horarioSelecionado);
                startActivity(it);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        String nomeBairro = intent.getStringExtra("Bairro_Selecionado");
        UbertinerDAO dao = new UbertinerDAO(this);
        List<Horario> horario = dao.buscaOnibusBairro(nomeBairro);
        dao.close();
        if(horario == null){
            Toast.makeText(this, "Nao ha dados cadastrados para " + nomeBairro, Toast.LENGTH_LONG).show();
            Intent in = new Intent(OnibusBairroActivity.this, TelaPrincipal.class);
            startActivity(in);
        }else {
            this.adapterBairro = new ArrayAdapter<Horario>(this, adapterLayout, horario);
            this.lv_detalhebairros.setAdapter(adapterBairro);
        }
    }
}
