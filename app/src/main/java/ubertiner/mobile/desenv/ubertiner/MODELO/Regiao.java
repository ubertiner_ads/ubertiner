package ubertiner.mobile.desenv.ubertiner.MODELO;

/**
 * Created by SamuelMiranda on 01/05/2016.
 */
public class Regiao {

    private String nomeRegiao;
    private String nomeCidade;

    public Regiao() {
    }

    public Regiao(String nomeRegiao, String nomeCidade) {
        this.nomeRegiao = nomeRegiao;
        this.nomeCidade = nomeCidade;
    }

    @Override
    public String toString() {
        return "Regiao{" +
                "nomeRegiao='" + nomeRegiao + '\'' +
                ", nomeCidade='" + nomeCidade + '\'' +
                '}';
    }

    public String getNomeRegiao() {
        return nomeRegiao;
    }

    public void setNomeRegiao(String nomeRegiao) {
        this.nomeRegiao = nomeRegiao;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }
}
