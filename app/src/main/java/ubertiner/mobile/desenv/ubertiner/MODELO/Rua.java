package ubertiner.mobile.desenv.ubertiner.MODELO;

import java.io.Serializable;

/**
 * Created by SamuelMiranda on 01/05/2016.
 */
public class Rua implements Serializable{

    private String nomeRua;
    private String nomeBairro;

    public Rua(String nomeRua, String nomeBairro) {
        this.nomeRua = nomeRua;
        this.nomeBairro = nomeBairro;
    }

    public Rua() {
    }

    @Override
    public String toString() {
        return "Rua{" +
                "nomeRua='" + nomeRua + '\'' +
                ", nomeBairro='" + nomeBairro + '\'' +
                '}';
    }

    public String getNomeRua() {
        return nomeRua;
    }

    public void setNomeRua(String nomeRua) {
        this.nomeRua = nomeRua;
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }
}
