package ubertiner.mobile.desenv.ubertiner;

/**
 * Created by SamuelMiranda on 24/04/2016.
 */
public class ScriptSQL {

    private String sqlCIDADE;
    private String sqlInsertCIDADE;
    private String sqlUSUARIO;
    private String sqlInsertUSUARIO;
    private String sqlREGIAO;
    private String sqlInsertREGIAO;
    private String sqlBAIRRO;
    private String sqlInsertBAIRRO;
    private String sqlRUA;
    private String sqlInsertRUA;
    private String sqlONIBUS;
    private String sqlInsertONIBUS;
    private String sqlHORARIO;
    private String sqlInsertHORARIO;

    //-------------------------------------------//
    public ScriptSQL() {
        this.sqlCIDADE = "\n" +
                "CREATE TABLE IF NOT EXISTS CIDADE (\n" +
                "  nomeCidade VARCHAR(45) NOT NULL PRIMARY KEY,\n" +
                "  estado VARCHAR(2) NULL\n" +
                "   );";


        this.sqlInsertCIDADE =  "insert INTO CIDADE VALUES ('Uberaba', 'Minas Gerais'); ";

        this.sqlUSUARIO = "CREATE TABLE IF NOT EXISTS USUARIO (\n" +
                "  _id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "  nome VARCHAR(45) NULL UNIQUE ON CONFLICT ROLLBACK,\n" +
                "  senha VARCHAR(45) NULL,\n" +
                "  nomeCidade VARCHAR(45) NOT NULL,"+
                "  tutorialInicial BOOLEAN NULL," +
                "  sessaoAtiva BOOLEAN NULL," +
                "  CONSTRAINT fk_usuario_cidade\n" +
                "    FOREIGN KEY (nomeCidade)\n" +
                "    REFERENCES CIDADE (nomeCidade)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION);";

        this.sqlREGIAO = "CREATE TABLE IF NOT EXISTS REGIAO (\n" +
                "  nomeRegiao VARCHAR(30) NOT NULL PRIMARY KEY ,\n" +
                "  nomeCidade VARCHAR(45) NOT NULL,\n" +
                "  CONSTRAINT fk_bairros_cidade1\n" +
                "    FOREIGN KEY (nomeCidade)\n" +
                "    REFERENCES CIDADE (nomeCidade)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION);";

        this.sqlInsertREGIAO = "insert INTO REGIAO VALUES('Norte','Uberaba')," +
                "('Sul','Uberaba')," +
                "('Leste','Uberaba')," +
                "('Oeste','Uberaba')," +
                "('Central','Uberaba');";

        this.sqlBAIRRO = "CREATE TABLE IF NOT EXISTS BAIRRO (\n" +
                "  nomeBairro VARCHAR(50) NOT NULL PRIMARY KEY,\n" +
                "  nomeRegiao VARCHAR(30) NOT NULL,\n" +
                "  CONSTRAINT fk_bairros_regiao1 \n" +
                "    FOREIGN KEY (nomeRegiao) \n" +
                "    REFERENCES REGIAO (nomeRegiao)\n" +
                "    ON DELETE NO ACTION \n" +
                "    ON UPDATE NO ACTION);";

        this.sqlInsertBAIRRO =  "insert INTO BAIRRO VALUES('Recanto da Terra','Norte')," +
                "('Recanto das Torres','Norte')," +
                "('Recreio','Norte')," +
                "('Residencial Antonia Cândida','Norte')," +
                "('Residencial 2.000','Norte')," +
                "('Residencial Estados Unidos','Norte')," +
                "('Residencial Mangueiras','Norte')," +
                "('Residencial Mônica Cristina','Norte')," +
                "('Residencial Monte Castelo','Norte')," +
                "('Residencial Morada Du Park','Norte')," +
                "('Residencial Parque Dos Girassóis','Norte')," +
                "('Santa Inez','Norte')," +
                "('Santa Maria','Norte')," +
                "('Santa Marta','Norte')," +
                "('Serra do Sol','Norte')," +
                "('Sete Colinas','Norte')," +
                "('Titã Rezende','Norte')," +
                "('Jardim Elza Amui I','Norte')," +
                "('Jardim Elza Amui II','Norte')," +
                "('Jardim Elza Amui III','Norte')," +
                "('Jardim Elza Amui IV','Norte')," +
                "('Josa Bernadino I','Norte')," +
                "('Josa Bernadino II','Norte')," +

                "('Chácara Santa Clara','Sul')," +
                "('Chácara Vale do Sol','Sul')," +
                "('Parque das Américas','Sul')," +
                "('Parque Gameleira','Sul')," +
                "('Parque Mirante','Sul')," +
                "('Parque São José','Sul')," +
                "('Planalto','Sul')," +
                "('Pontal','Sul')," +
                "('Quinta da Boa Esperança','Sul')," +
                "('Leblon','Sul')," +
                "('Jardim Manhatan','Sul')," +
                "('Jardim Maracanã','Sul')," +
                "('Jardim Metrópole','Sul')," +
                "('Jardim Morumbi','Sul')," +
                "('Cidade Jardim','Sul')," +
                "('Cidade Nova','Sul')," +
                "('Cidade Ozanan','Sul')," +
                "('Conjunto Boa Vista','Sul')," +
                "('Conjunto Habitacional Uberaba','Sul')," +
                "('Conjunto José Barbosa','Sul')," +
                "('Conjunto Margarida Azevedo','Sul')," +
                "('Conjunto Morada do Sol','Sul')," +
                "('Conjunto Silvério Cartafina','Sul')," +
                "('Costa Teles','Sul')," +
                "('Costa Teles II','Sul')," +

                "('Tutunas','Leste')," +
                "('Umuarama','Leste')," +
                "('Universitário','Leste')," +
                "('Vila Celeste','Leste')," +
                "('Vila Esperança','Leste')," +
                "('Vila Leandro','Leste')," +
                "('Vila Maria Helena','Leste')," +
                "('Vila Militar','Leste')," +
                "('Vila Olímpica','Leste')," +
                "('Vila Presidente Vargas','Leste')," +
                "('Vila Raquel','Leste')," +
                "('Vila Sao Cristovao','Leste')," +
                "('Vila Sao José','Leste')," +
                "('Vila Sao Vicente','Leste')," +
                "('Vilagio Di Fiori','Leste')," +
                "('Manoel Mendes','Leste')," +
                "('Maringá','Leste')," +
                "('Jardim Elza AmuiL I','Leste')," +
                "('Jardim Elza AmuiL II','Leste')," +
                "('Jardim Elza AmuiL III','Leste')," +
                "('Jardim Elza AmuiL IV','Leste')," +

                "('Santos Dumond','Oeste')," +
                "('São Geraldo','Oeste')," +
                "('Serra Dourada','Oeste')," +
                "('Beija Flor I','Oeste')," +
                "('Beija Flor II','Oeste')," +
                "('Morada das Fontes','Oeste')," +
                "('Nossa Senhora Aparecida','Oeste')," +
                "('Nossa Senhora de Lourdes','Oeste')," +
                "('Olinda','Oeste')," +
                "('Pacaembu','Oeste')," +
                "('Alfredo Freire I','Oeste')," +
                "('Alfredo Freire II','Oeste')," +
                "('Alfredo Freire III','Oeste')," +
                "('Parque dos Girassóis','Oeste')," +
                "('Jardim Copacabana','Oeste')," +
                "('Francisco Angotti','Oeste')," +
                "('Grande Horizonte','Oeste')," +
                "('Jockey Parque','Oeste')," +
                "('Domingos Mazeta','Oeste')," +
                "('Estados Unidos','Oeste')," +
                "('Euro Park','Oeste')," +
                "('Fabrício','Oeste')," +
                "('Frei Eugenio','Oeste')," +
                "('Flamboyant Residencial Pakr','Oeste')," +
                "('Gameleiras II','Oeste')," +
                "('Gleba Santa Mônica I','Oeste')," +
                "('Gleba Santa Mônica II','Oeste')," +
                "('Guanabara Jardim Esplanada ','Oeste')," +
                "('Jardim Induberaba','Oeste')," +
                "('Jardim Italia','Oeste')," +
                "('Jardim do Lago','Oeste')," +
                "('Jardim Nenê Gomes','Oeste')," +
                "('Jardim Oneida Mendes','Oeste')," +
                "('Jardim Primavera','Oeste')," +
                "('Jardim Santa Adélia','Oeste')," +
                "('Jardim São Bento','Oeste')," +
                "('Jardim Siriema','Oeste')," +
                "('Jardim Terra Santa','Oeste')," +
                "('Jardim Triângulo','Oeste')," +
                "('Hyléia','Oeste')," +
                "('Indianápolis','Oeste')," +
                "('Irmãos Soares','Oeste')," +
                "('Jardim Alexandre Campos','Oeste')," +
                "('Jardim Alvorada I','Oeste')," +
                "('Jardim América','Oeste')," +
                "('Jardim Bela Vista','Oeste')," +
                "('Jardim Califórnia','Oeste')," +
                "('Jardim Canadá','Oeste')," +
                "('Jardim Eldorado','Oeste')," +
                "('Distrito Industrial I','Oeste')," +


                "('Centro','Central')," +
                "('Mercês','Central')," +
                "('Estados unidos','Central')," +
                "('Grande São Benedito','Central')," +
                "('Cássio Rezende','Central');";


        this.setSqlRUA("CREATE TABLE IF NOT EXISTS RUA (\n" +
                "  nomeRua VARCHAR(50) NOT NULL PRIMARY KEY,\n" +
                "  nomeBairro VARCHAR(30) NOT NULL ,\n" +
                "  CONSTRAINT fk_RUA_BAIRRO1\n" +
                "    FOREIGN KEY (nomeBairro)\n" +
                "    REFERENCES BAIRRO (nomeBairro)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION);");

        this.sqlInsertRUA = "insert INTO RUA VALUES('Avenida Um','Recanto da Terra'),('Rua Dois','Recanto da Terra'),"+

                "('Alameda Real','Recanto das Torres')," +
                "('Alameda Colonial','Recanto das Torres')," +

                "('Avenida Filomena Cartafina','Recreio'),"+
                "('Rua A','Recreio')," +

                "('Rua Eva das Graças Oliveira Silva','Residencial Antonia Cândida')," +
                "('Rua Alzira Curi Cauhy','Residencial Antonia Cândida')," +

                "('Rua Olinto Pedro Magalhães','Residencial 2000')," +
                "('Rua Seis','Residencial 2000')," +

                "('Rua Virgínia','Residencial Estados Unidos')," +
                "('Rua Flórida','Residencial Estados Unidos')," +

                "('Rua José Rodrigues de Almeida','Residencial Mangueiras')," +
                "('Avenida Guilherme Capucci','Residencial Mangueiras')," +

                "('Rua José Justino Sobrinho','Residencial Mônica Cristina')," +
                "('Rua Oliveiro Nery da Silva','Residencial Mônica Cristina')," +

                "('Avenida Salomão Abdanur','Residencial Monte Castelo')," +
                "('Rua Irineu Teobaldo da Silva','Residencial Monte Castelo')," +

                "('Rua Wanda Alves Pereira','Residencial Morada Du Park')," +
                "('Rua Oito','Residencial Morada Du Park')," +

                "('Avenida José Geraldo de Souza','Residencial Parque dos Girassóis')," +
                "('Rua Praia do Forte','Residencial Parque dos Girassóis')," +

                "('Rua Amapá','Santa Inez')," +
                "('Rua Manágua','Santa Inez')," +

                "('Rua Natal','Santa Maria')," +
                "('Rua Curitiba','Santa Maria')," +

                "('Avenida da Saudade','Santa Marta')," +
                "('Alameda Dona Fiica','Santa Marta')," +

                "('Rua Henrique Guillaumon','Serra do Sol')," +
                "('Rua Aristides Ferreira Toscano','Serra do Sol')," +

                "('Rua Doutor Sebastião Fleury','Sete Colinas')," +
                "('Rua Ângelo Gutierrez','Sete Colinas')," +

                "('Rua Nelson Barbosa','Titã Rezende')," +

                "('Rua Januário Mário Felice','Jardim Elza Amui I')," +
                "('Rua Theodoro Júliano','Jardim Elza Amui I')," +

                "('Rua Sebastião Rossi','Jardim Elza Amui II')," +
                "('Rua Oditi Antunes de Abreu','Jardim Elza Amui II')," +

                "('Rua Sebastiana Divina Nascimento Ventura','Jardim Elza Amui III')," +
                "('Rua Manoel Gomes Caiado','Jardim Elza Amui III')," +

                "('Rua Banuta Moisés','Jardim Elza Amui IV')," +
                "('Rua Enelzira Maria Silva','Jardim Elza Amui IV')," +

                "('Rua Alfredo Afonso da Silva','Josa Bernadino I')," +
                "('Rua José Inajá Morais Lorena','Josa Bernadino I')," +

                "('Rua João Coelho de Rezende','Josa Bernadino II')," +
                "('Rua Abner Teles Pereira','Josa Bernadino II')," +

                "('Rua Sete','Chácara Déa Maria')," +
                "('Rua Izaltina Borges de Araújo Andrade','Chácara Déa Maria')," +

                "('Avenida Nilda Oliveira Araújo','Chácara Vale do Sol')," +
                "('Rua D','Chácara Vale do Sol')," +

                "('Rua Delamare','Parque das Américas')," +
                "('Rua Antônio Ribeiro Silva','Parque das Américas')," +

                "('Rua Maria Vaz de Azevedo','Parque Gameleira')," +
                "('Rua César Boareto','Parque Gameleira')," +

                "('Rua José Pimenta Camargo','Parque Mirante'),"+
                "('Praça Elvira Capucci França','Parque Mirante'),"+

                "('Rua Nelson Ciabotti','Parque São José'),"+
                "('Rua José Prazeres','Parque São José'),"+

                "('Rua Taguatinga','Planalto'),"+
                "('Rua Cruzeiro','Planalto'),"+

                "('Rua Sabiá','Pontal'),"+
                "('Rua Pardal','Pontal'),"+

                "('Rua Doutor José Puppo Felicíssimo','Quinta da Boa Esperança'),"+
                "('Rua Frei Martinho Benet','Quinta da Boa Esperança'),"+

                "('Avenida Nelson Freire','Leblon'),"+
                "('Rua Nhô Bernardino','Leblon'),"+

                "('Rua Maurício Cury','Jardim Manhatan'),"+
                "('Rua José Mauro Cury','Jardim Manhatan'),"+

                "('Rua Homero Moreira Silva','Jardim Maracanã'),"+
                "('Rua Doutor Nivaldo Vilela Ramos','Jardim Maracanã'),"+

                "('Rua Campo Mourão','Jardim Metrópole'),"+
                "('Avenida José Vallim de Mello','Jardim Metrópole'),"+

                "('Rua Doutor Sílvio Cordeiro Oliveira','Jardim Morumbi'),"+
                "('Rua Odair Santos','Jardim Morumbi'),"+

                "('Rua Felício Frange','Cidade Jardim'),"+
                "('Rua João Miguel Hueb','Cidade Jardim'),"+

                "('Rua Antônio Andyroba Barreto','Cidade Nova'),"+
                "('Rua Geraldo Borges de Araújo','Cidade Nova'),"+

                "('Rua Célio de Oliveira','Cidade Ozanan'),"+
                "('Rua Luciano Coelho da Silva','Cidade Ozanan'),"+

                "('Rua Sétimo Boscolo','Conjunto Boa Vista'),"+
                "('Avenida Henrique Castejon','Conjunto Boa Vista'),"+

                "('Rua Paula Metidieiro Masson','Conjunto Habitacional Uberaba'),"+
                "('Rua Geraldo Facury','Conjunto Habitacional Uberaba'),"+

                "('Rua Maria Batista de Andrade','Conjunto José Barbosa'),"+
                "('Rua Sinhá Barbosa','Conjunto José Barbosa'),"+

                "('Rua Maria Íris Veloso','Conjunto Margarida Azevedo'),"+
                "('Rua Normando Faro do Carmo','Conjunto Margarida Azevedo'),"+

                "('Rua Diocésio Pinheiro','Conjunto Morada do Sol'),"+
                "('Rua Hildebrando Ferreira de Morais','Conjunto Morada do Sol'),"+

                "('Rua Mauro Sabino Loés','Conjunto Silvério Cartafina'),"+
                "('Rua Alberto Ribeiro Almeida','Conjunto Silvério Cartafina'),"+

                "('Rua Doutor Daniel Mendes','Costa Teles'),"+
                "('Rua Militino José Pinto','Costa Teles'),"+

                "('Alameda Doutor José Antônio Gerolin','Costa Teles II'),"+
                "('Rua Joaquim José da Costa','Costa Teles II'),"+

                "('Avenida Alfredo de Faria','Tutunas'),"+
                "('Rua Francisco Rosa','Tutunas'),"+

                "('Rua Alfa','Umuarama'),"+
                "('Rua Gama','Umuarama'),"+

                "('Rua Orlando de Paiva Abreu','Universitário'),"+
                "('Rua Vinícius de Morais','Universitário'),"+

                "('Rua Voluntário Ramiro Teles','Vila Celeste'),"+
                "('Rua Oriente','Vila Celeste'),"+

                "('Rua Luís José da Costa','Vila Esperança'),"+
                "('Rua Ângelo Bevilácqua','Vila Esperança'),"+

                "('Rua Alaor Prata','Vila Leandro'),"+
                "('Rua Lauro Borges','Vila Leandro'),"+

                "('Rua Paraiso','Vila Maria Helena'),"+
                "('Rua Dona Rafa Cecílio','Vila Maria Helena'),"+

                "('Rua do Trabalhador','Vila Militar'),"+
                "('Rua Dona Laura','Vila Militar'),"+

                "('Avenida Gustavo Rodrigues da Cunha','Vila Olímpica'),"+
                "('Avenida Gabriela Castro Cunha','Vila Olímpica'),"+

                "('Rua Palmira da Costa Escoura','Vila Presidente Vargas'),"+

                "('Avenida Deputado José Marcus Cherem','Vila São Cristovão'),"+
                "('Rua Ângelo Fuzaro','Vila São Cristovão'),"+

                "('Rua Mem de Sá','Vila São José'),"+
                "('Rua Imperador','Vila São José'),"+

                "('Rua São Martim','Vila São Vicente'),"+
                "('Rua Santo Amaro','Vila São Vicente'),"+

                "('Rua Adília de Novaes Franca','Vilagio Di Fiori'),"+
                "('Rua Joana D Arc Santos','Vilagio Di Fiori'),"+

                "('Rua João Bento de Carvalho','Manoel Mendes'),"+
                "('Rua Comendador Nilton Val Ribeiro','Manoel Mendes'),"+

                "('Rua Antônio Alves Pinto Filho','Maringá'),"+
                "('Rua Alexandre Amâncio de Souza','Maringá'),"+

                "('Rua Marcos Lombardi','Santos Dumond'),"+
                "('Rua Pierre Elias Guimarães','Santos Dumond'),"+

                "('Rua Melvin Jones','São Geraldo'),"+
                "('Rua Joaquim Adriano','São Geraldo'),"+

                "('Rua Antônio Bernardes da Silva','Serra Dourada'),"+
                "('Rua Jaime Rodrigues Bernardes','Serra Dourada'),"+

                "('Rua Tereza Magalhães Matos','Beija Flor I'),"+
                "('Rua Francisco Barto de Menezes','Beija Flor I'),"+

                "('Rua Floriza Fontoura Rosa','Beija Flor II'),"+
                "('Rua Francisco Antônio Rosa','Beija Flor II'),"+

                "('Avenida Doutor Eurípedes Cordeiro','Morada das Fontes'),"+
                "('Alameda das Camélias','Morada das Fontes'),"+

                "('Rua Monsenhor Inácio','Nossa Senhora Aparecida'),"+
                "('Rua Aimorés','Nossa Senhora Aparecida'),"+

                "('Rua das Laranjeiras','Nossa Senhora de Lourdes'),"+
                "('Rua das Cajazeiras','Nossa Senhora de Lourdes'),"+

                "('Rua Edgar Vidal Leite Ribeiro','Olinda'),"+
                "('Rua Edmundo Silveira','Olinda'),"+
                "('Avenida Dona Maria de Santana Borges','Olinda'),"+

                "('Rua Joaquim Fernandes da Silva','Pacaembu'),"+
                "('Rua Cacildo Pereira da Silva','Pacaembu'),"+

                "('Avenida Alceu Lyrio','Alfredo Freire I'),"+
                "('Avenida Doutor Eduardo Tahan','Alfredo Freire I'),"+
                "('Rua Carolina Pucci Molinar','Alfredo Freire I'),"+
                "('Rua José Gonçalves Borges','Alfredo Freire I'),"+
                "('Avenida Joaquim Borges Assunção','Alfredo Freire I'),"+
                "('Rua Laerte Rodrigues Borges','Alfredo Freire I'),"+

                "('Rua B','Alfredo Freire II'),"+
                "('Praça D','Alfredo Freire II'),"+
                "('Rua Mônica Machiyama','Alfredo Freire II'),"+


                "('Avenida João Moreira da Silva','Alfredo Freire III'),"+
                "('Rua Wilson Antônio Rodrigues','Alfredo Freire III'),"+

                "('Rua Dez','Parque dos Girassóis'),"+
                "('Rua Nove','Parque dos Girassóis'),"+

                "('Rua Maragogi','Jardim Copacabana'),"+
                "('Rua Mangaratiba','Jardim Copacabana'),"+

                "('Alameda Verde','Grande Horizonte'),"+
                "('Alameda Lilás','Grande Horizonte'),"+

                "('Avenida Aluízio de Mello','Jockey Parque'),"+
                "('Avenida Lauro Fontoura Júnior','Jockey Parque'),"+

                "('Rua José Bento Alves','Estados Unidos'),"+
                "('Rua Raimundo Soares Azevedo','Estados Unidos'),"+

                "('Rua Georgeta de Ávila Pimenta','Euro Park'),"+

                "('Rua Tiradentes','Fabrício'),"+
                "('Rua Major Miguel Martins Ferreira','Fabrício'),"+

                "('Rua Alzira Jacob Hercos','Frei Eugenio'),"+
                "('Rua Aurélio Staciarini','Frei Eugenio'),"+

                "('Rua Moura Teles','Jardim América'),"+
                "('Rua Miguel Veríssimo','Jardim América'),"+

                "('Rua Ambrosina Faria','Jardim Italia'),"+
                "('Rua Ana Trezi de Almeida','Jardim Italia'),"+

                "('Rua Bolivar de Oliveira','Jardim São Bento'),"+
                "('Rua Alaor Nassif Miziara','Jardim São Bento'),"+

                "('Rua João Caetano','Centro'),"+
                "('Avenida Guilherme Ferreira','Centro'),"+

                "('Rua Abdon Alonso Y Alonso','Cássio Rezende'),"+
                "('Rua Abel Santos Anjo','Cássio Rezende'),"+

                "('Rua Engenheiro Foze Kalil Abrahão','Mercês'),"+
                "('Rua Gastão Vieira de Souza','Mercês'),"+
                "('Rua Tenente Coronel Bento Ferreira','Mercês'),"+

                "('Rua Afonso Riccioppo','Distrito Industrial I'),"+
                "('Avenida Francisco Podboy','Distrito Industrial I'),"+
                "('Avenida de Bernardo Seibel','Distrito Industrial I'),"+
                "('Avenida Santos Guido','Distrito Industrial I'),"+

                "('Rua Valdemar Alves','Jardim Primavera'),"+
                "('Rua Laurico Miguel de Farias','Jardim Primavera');";



        this.sqlONIBUS = "CREATE TABLE IF NOT EXISTS ONIBUS (\n" +
                "  codigo INTEGER PRIMARY KEY,\n" +
                "  nomeOnibus VARCHAR(50) NOT NULL,\n" +
                "  empresa VARCHAR(30) NOT NULL  \n" +
                ");";

        this.sqlInsertONIBUS = "insert INTO ONIBUS VALUES(1,'Jardim Italia','Transube')," +
                "(2,'Alfredo Freire','Transube');";


        this.sqlHORARIO = "CREATE TABLE IF NOT EXISTS HORARIO (\n" +
                "  nomeOnibus VARCHAR(50) NOT NULL,\n" +
                "  nomeCidade VARCHAR(45) NOT NULL,\n" +
                "  nomeRegiao VARCHAR(30) NULL,\n" +
                "  nomeBairro VARCHAR(30) NOT NULL,\n" +
                "  nomeRua VARCHAR(50) NOT NULL,\n" +
                "  tempo VARCHAR(45) NOT NULL,\n" +
                "  latitude NUMERIC(5,5) NULL,\n" +
                "  longitude NUMERIC(5,5) NULL,\n" +
                "  CONSTRAINT fk_RUA_has_ONIBUS_RUA1\n" +
                "    FOREIGN KEY (nomeRua)\n" +
                "    REFERENCES RUA (nomeRua)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION,\n" +
                "  CONSTRAINT fk_RUA_has_ONIBUS_ONIBUS1\n" +
                "    FOREIGN KEY (nomeOnibus)\n" +
                "    REFERENCES ONIBUS (nomeOnibus)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION,\n" +
                "  CONSTRAINT fk_RUA_has_ONIBUS_BAIRRO1\n" +
                "    FOREIGN KEY (nomeBairro)\n" +
                "    REFERENCES BAIRRO (nomeBairro)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION,\n" +
                "  CONSTRAINT fk_RUA_has_ONIBUS_REGIAO1\n" +
                "    FOREIGN KEY (nomeRegiao)\n" +
                "    REFERENCES REGIAO (nomeRegiao)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION,\n" +
                "  CONSTRAINT fk_RUA_has_ONIBUS_CIDADE1\n" +
                "    FOREIGN KEY (nomeCidade)\n" +
                "    REFERENCES CIDADE (nomeCidade)\n" +
                "    ON DELETE NO ACTION\n" +
                "    ON UPDATE NO ACTION);\n";

        this.sqlInsertHORARIO = "insert INTO HORARIO VALUES('Jardim Italia','Uberaba','Oeste','Jardim Italia','Rua Ana Trezi de Almeida','21:00', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Avenida Guarapuava','21:30', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Rua Yolanda Motta Leite','21:40', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Rua Yolanda Motta Leite','22:02', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Rua Yolanda Motta Leite','22:02', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Avenida Umuarama','22:02', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Avenida Umuarama','22:03', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Jardim América','Avenida Jose Vallim de Melo','22:03', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Jardim América','Avenida Jose Vallim de Melo','22:04', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Rua Maringa','22:04', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Rua Maringa','22:05', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Rua Atilio Ângelo de Paula','22:05', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Rua Atilio Angelo de Paula','22:06', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Avenida Nossa Senhora de Lourdes','22:06', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Avenida Nossa Senhora de Lourdes','22:06', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Avenida Nossa Senhora de Lourdes','22:07', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Avenida Nossa Senhora de Lourdes','22:07', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Avenida Jose Vallim de Melo','22:08', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Avenida Bandeirantes','22:09', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Avenida Bandeirantes','22:10', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','São Geraldo','Avenida Manoel de Melo Rezende','22:11', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Rua Presidente Washington Luiz','22:11', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Rua Presidente Washington Luiz','22:12', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Parque Gameleiras','Rua Presidente Washington Luiz','22:12', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Vila São Cristovão','Avenida Deputado José Marcus Cherem','22:13', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Vila São Cristovão','Avenida Deputado José Marcus Cherem','22:13', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Vila São Cristovão','Avenida Deputado José Marcus Cherem','22:13', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Vila São Cristovão','Avenida Deputado José Marcus Cherem','22:14', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Vila São Cristovão','Rua Orlando Vieira do Nascimento','22:15', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Vila São Cristovão','Rua Manoel Gonçalves Rezende','22:15', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Vila São Cristovão','Rua Manoel Gonçalves Rezende','22:15', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Vila São Cristovão','Rua Antônio Salge','22:16', '', ''),"+
                "('Jardim Italia','Uberaba','Oeste','Vila São Cristovão','Avenida Deputado José Marcus Cherem','22:17', '', ''),"+
                "('Jardim Italia','Uberaba','Central','Centro','Avenida Guilherme Ferreira','22:17', '-19.759043381628814', '-47.93566725276338'),"+
                "('Jardim Italia','Uberaba','Central','Centro','Avenida Guilherme Ferreira','22:18', '-19.759679500861697', '-47.93524614594804' ),"+
                "('Jardim Italia','Uberaba','Central','Centro','Avenida Guilherme Ferreira','22:18', '-19.759098915948645', '-47.93523005269395' ),"+
                "('Jardim Italia','Uberaba','Central','Centro','Avenida Guilherme Ferreira','22:19', '-19.75859910637421', '-47.93510667107927' ),"+
                "('Jardim Italia','Uberaba','Central','Centro','Avenida Guilherme Ferreira','22:20', '-19.75766006596874', '-47.934709704145064' ),"+
                "('Jardim Italia','Uberaba','Central','Centro','Avenida Guilherme Ferreira','22:20', '-19.757377343043512', '-47.93461850903856' ),"+
                "('Jardim Italia','Uberaba','Central','Centro','Avenida Guilherme Ferreira','22:21', '-19.75639285751855', '-47.93439320348131' ),"+
                "('Jardim Italia','Uberaba','Central','Centro','Avenida Guilherme Ferreira','22:21', '-19.754883301246824', '-47.934178626760115' ),"+
                "('Jardim Italia','Uberaba','Central','Centro','Avenida Guilherme Ferreira','22:22', '-19.753449461996155', '-47.93399087212907' ),"+
                "('Jardim Italia','Uberaba','Central','Centro','Praça Manoel Terra','22:23', '', ''),"+
                "('Jardim Italia','Uberaba','Central','Centro','Rua Alaor Prata','22:24', '', ''),"+
                "('Jardim Italia','Uberaba','Central','Centro','Rua Governador Valadares','22:26', '', ''),"+
                "('Jardim Italia','Uberaba','Central','Centro','Rua Governador Valadares','22:28', '', ''),"+
                "('Jardim Italia','Uberaba','Central','Estados unidos','Avenida Leopoldino de Oliveira','22:30', '', '')," +


                "('Alfredo Freire','Uberaba','Oeste','Alfredo Freire II','Rua Mônica Machiyama','20:02', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Alfredo Freire I','Rua Carolina Pucci Molinar','20:06', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Alfredo Freire I','Avenida Alceu Lyrio','20:06', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Alfredo Freire I','Avenida Alceu Lyrio','20:07', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Alfredo Freire I','Rua José Gonçalves Borges','20:07', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Alfredo Freire I','Rua José Gonçalves Borges','20:08', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Alfredo Freire I','Avenida Joaquim Borges Assunção','20:09', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Alfredo Freire I','Rua Laerte Rodrigues Borges','20:09', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Distrito Industrial I','Rua Afonso Riccioppo','20:09', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Distrito Industrial I','Avenida Francisco Podboy','20:09', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Distrito Industrial I','Avenida Francisco Podboy','20:10', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Distrito Industrial I','Avenida de Bernardo Seibel','20:11', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Distrito Industrial I','Avenida Santos Guido','20:12', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Distrito Industrial I','Avenida Francisco Podboy','20:13', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Olinda','Avenida Dona Maria de Santana Borges','20:14', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Olinda','Avenida Dona Maria de Santana Borges','20:15', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Olinda','Avenida Dona Maria de Santana Borges','20:15', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Olinda','Avenida Dona Maria de Santana Borges','20:16', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Olinda','Avenida Dona Maria de Santana Borges','20:16', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Olinda','Avenida Dona Maria de Santana Borges','20:17', '', '')," +
                "('Alfredo Freire','Uberaba','Oeste','Olinda','Avenida Dona Maria de Santana Borges','20:17', '', '')," +
                "('Alfredo Freire','Uberaba','Norte','Santa Marta','Avenida da Saudade','20:18', '', '')," +
                "('Alfredo Freire','Uberaba','Norte','Santa Marta','Avenida da Saudade','20:18', '', '')," +
                "('Alfredo Freire','Uberaba','Norte','Santa Marta','Avenida da Saudade','20:19', '', '')," +
                "('Alfredo Freire','Uberaba','Norte','Santa Marta','Avenida da Saudade','20:20', '', '')," +
                "('Alfredo Freire','Uberaba','Central','Mercês','Rua Tenente Coronel Bento Ferreira','20:20', '', '')," +
                "('Alfredo Freire','Uberaba','Central','Mercês','Rua Tenente Coronel Bento Ferreira','20:35', '', '')," +
                "('Alfredo Freire','Uberaba','Central','Mercês','Rua Tenente Coronel Bento Ferreira','20:45', '', '')," +
                "('Alfredo Freire','Uberaba','Central','Estados unidos','Avenida Leopoldino de Oliveira','20:55', '', '');";
    }


    public String getSqlCIDADE() {
        return sqlCIDADE;
    }

    public void setSqlCIDADE(String sqlCIDADE) {
        this.sqlCIDADE = sqlCIDADE;
    }

    public String getSqlInsertCIDADE() {
        return sqlInsertCIDADE;
    }

    public void setSqlInsertCIDADE(String sqlInsertCIDADE) {
        this.sqlInsertCIDADE = sqlInsertCIDADE;
    }

    public String getSqlUSUARIO() {
        return sqlUSUARIO;
    }

    public void setSqlUSUARIO(String sqlUSUARIO) {
        this.sqlUSUARIO = sqlUSUARIO;
    }

    public String getSqlREGIAO() {
        return sqlREGIAO;
    }

    public void setSqlREGIAO(String sqlREGIAO) {
        this.sqlREGIAO = sqlREGIAO;
    }

    public String getSqlBAIRRO() {
        return sqlBAIRRO;
    }

    public void setsqlBAIRRO(String sqlBAIRRO) {
        this.sqlBAIRRO = sqlBAIRRO;
    }

    public String getsqlInsertRUA() {return sqlInsertRUA; }

    public void setsqlInsertRUA(String sqlInsertRUA) {
        this.sqlInsertRUA = sqlInsertRUA;
    }

    public String getSqlONIBUS() {
        return sqlONIBUS;
    }

    public void setSqlONIBUS(String sqlONIBUS) {
        this.sqlONIBUS = sqlONIBUS;
    }

    public String getSqlHORARIO() {
        return sqlHORARIO;
    }

    public void setSqlHORARIO(String sqlHORARIO) {
        this.sqlHORARIO = sqlHORARIO;
    }

    public void teste(){

    }

    public String getSqlInsertREGIAO() {
        return sqlInsertREGIAO;
    }

    public void setSqlInsertREGIAO(String sqlInsertREGIAO) {
        this.sqlInsertREGIAO = sqlInsertREGIAO;
    }

    public String getSqlInsertBAIRRO() {
        return sqlInsertBAIRRO;
    }

    public void setSqlInsertBAIRRO(String sqlInsertBAIRRO) {
        this.sqlInsertBAIRRO = sqlInsertBAIRRO;
    }

    public String getSqlInsertUSUARIO() {
        return sqlInsertUSUARIO;
    }

    public void setSqlInsertUSUARIO(String sqlInsertUSUARIO) {
        this.sqlInsertUSUARIO = sqlInsertUSUARIO;
    }

    public String getSqlRUA() {
        return sqlRUA;
    }

    public void setSqlRUA(String sqlRUA) {
        this.sqlRUA = sqlRUA;
    }

    public String getSqlInsertONIBUS() {
        return sqlInsertONIBUS;
    }

    public void setSqlInsertONIBUS(String sqlInsertONIBUS) {
        this.sqlInsertONIBUS = sqlInsertONIBUS;
    }

    public String getSqlInsertHORARIO() {
        return sqlInsertHORARIO;
    }

    public void setSqlInsertHORARIO(String sqlInsertHORARIO) {
        this.sqlInsertHORARIO = sqlInsertHORARIO;
    }
}
