package ubertiner.mobile.desenv.ubertiner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginAdmin extends AppCompatActivity {

    UbertinerDAO dao = new UbertinerDAO(this);
    Intent in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_admin);
    }

    public void validaLogin(View view){

        EditText edt_usuario = (EditText) findViewById(R.id.edt_usuario_admin);
        EditText edt_senha = (EditText) findViewById(R.id.edt_senha_admin);
        String nome_usuario = edt_usuario.getText().toString();
        String senha = edt_senha.getText().toString();

        try {
            if (dao.validarAcessoAdmin(nome_usuario, senha)) {
                in = new Intent(LoginAdmin.this, MenuAdmin.class );

                startActivity(in);
            } else {
                Toast.makeText(this, "login admin invalido", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            Toast.makeText(this, "Houve um ERRO!   " +e, Toast.LENGTH_LONG).show();
        }
    }

    public void voltarLogin(){
        in = new Intent(LoginAdmin.this, TelaLogin.class );
        startActivity(in);
    }


}
