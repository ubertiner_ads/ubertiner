package ubertiner.mobile.desenv.ubertiner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.renderscript.Script;
import android.widget.Toast;

import java.security.PublicKey;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.MODELO.Bairro;
import ubertiner.mobile.desenv.ubertiner.MODELO.Horario;
import ubertiner.mobile.desenv.ubertiner.MODELO.Onibus;
import ubertiner.mobile.desenv.ubertiner.MODELO.Usuario;

/**
 * Created by SamuelMiranda on 24/04/2016.
 */
public class UbertinerDAO extends SQLiteOpenHelper{

    private static final int VERSAO =1;

    /*TABELAS*/
        private static final String USUARIO = "USUARIO";
        private static final String CIDADE = "CIDADE";
        private static final String REGIAO = "REGIAO";
        private static final String BAIRRO = "BAIRRO";
        private static final String RUA = "RUA";
        private static final String HORARIO = "HORARIO";
        private static final String ONIBUS = "ONIBUS";
    /*TABELAS*/

    private static final String banco = "db_ubertiner";
    private static final String tag = "PROJETO UBERTINER";

        public UbertinerDAO(Context ctx){
            super(ctx, banco, null, VERSAO);
        }

        public UbertinerDAO(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            ScriptSQL banco= new ScriptSQL();

////////------------------BLOCO DE INICIALIZAÇÃO DO BANCO-----------//////////////////
            db.execSQL(banco.getSqlCIDADE());

            db.execSQL(banco.getSqlInsertCIDADE());

            db.execSQL(banco.getSqlUSUARIO());

//            db.execSQL(banco.getSqlInsertUSUARIO());

            db.execSQL(banco.getSqlREGIAO());

            db.execSQL(banco.getSqlInsertREGIAO());

            db.execSQL(banco.getSqlBAIRRO());

            db.execSQL(banco.getSqlInsertBAIRRO());
//
            db.execSQL(banco.getSqlRUA());

            db.execSQL(banco.getsqlInsertRUA());

            db.execSQL(banco.getSqlONIBUS());

            db.execSQL(banco.getSqlInsertONIBUS());

            db.execSQL(banco.getSqlHORARIO());

            db.execSQL(banco.getSqlInsertHORARIO());

/////////////////////////////////////////////////////////////////////////////////////

            ScriptAdmin scriptAdmin = new ScriptAdmin();
            db.execSQL(scriptAdmin.getSqlADMIN());
            db.execSQL(scriptAdmin.getSqlInsertADMIN());

////////------------------BLOCO DE INSERÇÃO DE VALORES NO BANCO-----------//////////////////

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            String sql ="drop table if exists CIDADE; " +
                        "drop table if exists USUARIO; " +
                        "drop table if exists REGIAO; " +
                        "drop table if exists BAIRRO; " +
                        "drop table if exists RUA; " +
                        "drop table if exists ONIBUS; " +
                        "drop table if exists HORARIO; ";

            onCreate(db);
        }

    public List<String> listarNomesCidades(){
        List<String> nomesCidades = new ArrayList<>();
        String sql = "select nomeCidade from CIDADE";

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);
        nomesCidades.add(" Selecione sua Cidade ");
        if(cursor.getCount() > 0){

            while (cursor.moveToNext()){
                nomesCidades.add(cursor.getString(0));
            }
        }else{
            return null;
        }

        return nomesCidades;
    }

    public String listarCidadeUsuario(String nome){
        String nomeCidade;
        String sql = "select nomeCidade from USUARIO where nome = '"+nome+"' ";

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);
        cursor.moveToNext();
                nomeCidade = cursor.getString(0);

        return nomeCidade;
    }

    public List<String> listarBairros(){
        List<String> nomeBairros = new ArrayList<>();
//        Bairro bairro = new Bairro();
        String sql = "select nomeBairro from "+BAIRRO+" order by nomeBairro";

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        while (cursor.moveToNext()) {
            nomeBairros.add(cursor.getString(0));
        }
        return nomeBairros;
    }
//

    public List<String> listarRuas(){
        List<String> nomeRuas = new ArrayList<>();
        String sql = "select nomeRua from "+RUA + " order by nomeRua";

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        while (cursor.moveToNext()) {
            nomeRuas.add(cursor.getString(0));
        }
        return nomeRuas;
    }

    public List<String> listarOnibus(){
        List<String> nomeOnibus = new ArrayList<>();
        String sql = "select nomeOnibus from "+ONIBUS;
        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        while (cursor.moveToNext()) {
            nomeOnibus.add(cursor.getString(0));
        }
        return nomeOnibus;
    }

    public List<Horario> listarHorarios(){
        List<Horario> horarios = new ArrayList<>();
        Horario horario ;
        String sql = "select nomeOnibus, nomeRua, tempo from "+HORARIO + " order by nomeOnibus";

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

            while (cursor.moveToNext()) {
            horario = new Horario();
            horario.setNomeOnibus(cursor.getString(0));
            horario.setNomeRua(cursor.getString(1));
            horario.setTempo_1(cursor.getString(2));

            horarios.add(horario);
        }
        return horarios;
    }

    public List<String> teste(){
        List<String> dados = new ArrayList<>();

        String sql = "select * from "+BAIRRO;

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        while (cursor.moveToNext()){
            dados.add(cursor.getString(0));
            dados.add(cursor.getString(1));
        }

        return dados;
    }

        public void cadastrarTESTE() {

            ContentValues valuesCIDADE = new ContentValues();

            valuesCIDADE.put("nomeCidade", "Uberlandia");
            valuesCIDADE.put("estado", "minasgerais");

            getWritableDatabase().insert(CIDADE, null, valuesCIDADE);
        }

    public boolean cadastrarUSUARIO(Usuario usuario) {

        ContentValues valuesUSUARIO = new ContentValues();

        valuesUSUARIO.put("nome", usuario.getNome());
        valuesUSUARIO.put("senha", usuario.getSenha());
        valuesUSUARIO.put("nomeCidade", usuario.getNomeCidade());
        valuesUSUARIO.put("tutorialInicial", "false");
        try {
            getWritableDatabase().insert(USUARIO, null, valuesUSUARIO);
            return true;
        }catch (SQLiteException ex){
            return false;
        }

    }


    public boolean validarAcesso(String usuario, String senha) {
        String sql = "Select * from USUARIO where nome = '"+usuario+"' and senha = '"+senha+"' ";
////            //classe utilizada para ler o resultado de um registro
        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        if(cursor.getCount() > 0){
            getWritableDatabase().execSQL("UPDATE USUARIO SET sessaoAtiva= 'true' WHERE nome='"+usuario+"'");
            return true;
        }
        else {
            return false;
        }
    }



    public List<Horario> buscaOnibusBairro(String nomeBairro){
        //AQUI EU FAÇO A BUSCA
        String sql = "Select * from HORARIO where nomeBairro = '"+ nomeBairro +"' ";

        List<Horario> horariosOnibus = new ArrayList<>();
        Horario horario;


        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        if(cursor.getCount() > 0){

            while (cursor.moveToNext()) {
                    //AQUI EU SETO O OBJETO DE HORARIO
                    horario = new Horario();
                    horario.setNomeOnibus(cursor.getString(0));
                    horario.setNomeCidade(cursor.getString(1));
                    horario.setNomeBairro(cursor.getString(3));
                    horario.setNomeRua(cursor.getString(4));
                    horario.setTempo_1(cursor.getString(5));

                    // JOGO O OBJETO NO LIST
                    horariosOnibus.add(horario);
            }

            //RETORNO O LIST PARA O VIEW
            return horariosOnibus;

        }else {
            return null;
        }
    }

    public List<Horario> buscaOnibusRua(String nomeRua){
        //AQUI EU FAÇO A BUSCA
        String sql = "Select * from HORARIO where nomeRua = '"+nomeRua+"' ";

        List<Horario> horariosOnibus = new ArrayList<>();
        Horario horario;


        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        if(cursor.getCount() > 0){

            while (cursor.moveToNext()) {
                //AQUI EU SETO O OBJETO DE HORARIO
                horario = new Horario();

                horario.setNomeOnibus(cursor.getString(0));
                horario.setNomeCidade(cursor.getString(1));
                horario.setNomeBairro(cursor.getString(3));
                horario.setNomeRua(cursor.getString(4));
                horario.setTempo_1(cursor.getString(5));

                // JOGO O OBJETO NO LIST
                horariosOnibus.add(horario);
            }

            //RETORNO O LIST PARA O VIEW
            return horariosOnibus;

        }else {
            return null;
        }
    }
    public List<Horario> buscaOnibusRegiao(String nomeRegiao){
        //AQUI EU FAÇO A BUSCA
        String sql = "Select * from HORARIO where nomeRegiao = '"+nomeRegiao+"' ";

        List<Horario> horariosOnibus = new ArrayList<>();
        Horario horario;


        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        if(cursor.getCount() > 0){

            while (cursor.moveToNext()) {
                //AQUI EU SETO O OBJETO DE HORARIO
                horario = new Horario();
                horario.setNomeOnibus(cursor.getString(0));
                horario.setNomeCidade(cursor.getString(1));
                horario.setNomeBairro(cursor.getString(3));
                horario.setNomeRua(cursor.getString(4));
                horario.setTempo_1(cursor.getString(5));

                // JOGO O OBJETO NO LIST
                horariosOnibus.add(horario);
            }

            //RETORNO O LIST PARA O VIEW
            return horariosOnibus;

        }else {
            return null;
        }
    }

    public List<Horario> buscaOnibusHorario(String tempo, String nomeOnibus){
        //AQUI EU FAÇO A BUSCA
        String sql = "Select * from HORARIO where tempo = '"+tempo+"' and nomeOnibus = '"+nomeOnibus+"' ";

        Horario horario;
        List<Horario> horariosOnibus = new ArrayList<>();

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        if(cursor.getCount() > 0){

            while (cursor.moveToNext()) {
                //AQUI EU SETO O OBJETO DE HORARIO
                horario = new Horario();
                horario.setNomeOnibus(cursor.getString(0));
                horario.setNomeCidade(cursor.getString(1));
                horario.setNomeBairro(cursor.getString(3));
                horario.setNomeRua(cursor.getString(4));
                horario.setTempo_1(cursor.getString(5));

                horariosOnibus.add(horario);
            }

            //RETORNO O LIST PARA O VIEW
            return horariosOnibus;

        }else {
            return null;
        }
    }

    public List<Horario> buscaOnibus(String nomeOnibus){
        //AQUI EU FAÇO A BUSCA
        String sql = "Select * from HORARIO where nomeOnibus = '"+nomeOnibus+"' ";

        List<Horario> horariosOnibus = new ArrayList<>();
        Horario horario;


        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        if(cursor.getCount() > 0){

            while (cursor.moveToNext()) {
                //AQUI EU SETO O OBJETO DE HORARIO
                horario = new Horario();
                horario.setNomeOnibus(cursor.getString(0));
                horario.setNomeCidade(cursor.getString(1));
                horario.setNomeBairro(cursor.getString(3));
                horario.setNomeRua(cursor.getString(4));
                horario.setTempo_1(cursor.getString(5));

                // JOGO O OBJETO NO LIST
                horariosOnibus.add(horario);
            }

            //RETORNO O LIST PARA O VIEW
            return horariosOnibus;

        }else {
            return null;
        }
    }

    public Horario buscaOnibusHorario2(String tempo, String nomeOnibus){
        //AQUI EU FAÇO A BUSCA
        String sql = "Select * from HORARIO where tempo = '"+tempo+"' and nomeOnibus = '"+nomeOnibus+"' ";

        Horario horario = new Horario();

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        if(cursor.getCount() > 0){

            while (cursor.moveToNext()) {
                //AQUI EU SETO O OBJETO DE HORARIO
                horario.setNomeOnibus(cursor.getString(0));
                horario.setNomeCidade(cursor.getString(1));
                horario.setNomeBairro(cursor.getString(3));
                horario.setNomeRua(cursor.getString(4));
                horario.setTempo_1(cursor.getString(5));
                System.out.printf("---> HORARIO ONIBUS LATITUDE: " + cursor.getString(6));
                System.out.printf("---> HORARIO ONIBUS LONGITUDE: " + cursor.getString(7));
                horario.setLatitude(cursor.getFloat(6));
                horario.setLongitude(cursor.getFloat(7));
            }

            //RETORNO O LIST PARA O VIEW
            return horario;

        }else {
            return null;
        }
    }


    /*------------------------------ BLOCO VALIDAÇÃO MENU ADMIN  ------------------------------------*/

    public List<String> listarNomesRegioes(){
        List<String> nomesRegioes = new ArrayList<>();
        String sql = "select nomeRegiao from REGIAO";

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);
        nomesRegioes.add(" Selecione uma Regiao ");
        if(cursor.getCount() > 0){

            while (cursor.moveToNext()){
                nomesRegioes.add(cursor.getString(0));
            }
        }else{
            return null;
        }

        return nomesRegioes;
    }

    public List<String> listarNomesBairros(){
        List<String> nomesBairros = new ArrayList<>();
        String sql = "select nomeBairro from BAIRRO";

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);
        nomesBairros.add(" Selecione um Bairro ");
        if(cursor.getCount() > 0){

            while (cursor.moveToNext()){
                nomesBairros.add(cursor.getString(0));
            }
        }else{
            return null;
        }

        return nomesBairros;
    }

    public List<String> listarNomesOnibus(){
        List<String> nomesOnibus = new ArrayList<>();
        String sql = "select nomeOnibus from ONIBUS";

        Cursor cursor = getReadableDatabase().rawQuery(sql, null);
        nomesOnibus.add(" Selecione um Onibus ");
        if(cursor.getCount() > 0){

            while (cursor.moveToNext()){
                nomesOnibus.add(cursor.getString(0));
            }
        }else{
            return null;
        }

        return nomesOnibus;
    }
    /*   --------     ATÉ AQUI        -----------  */



    public boolean cadastrarHORARIO(Horario horario) {

        ContentValues valuesHORARIO = new ContentValues();

        valuesHORARIO.put("nomeOnibus", horario.getNomeOnibus());
        valuesHORARIO.put("nomeCidade", horario.getNomeCidade());
        valuesHORARIO.put("nomeRegiao", horario.getNomeRegiao());
        valuesHORARIO.put("nomeBairro", horario.getNomeBairro());
        valuesHORARIO.put("nomeRua", horario.getNomeRua());
        valuesHORARIO.put("tempo", horario.getTempo_1());
        System.out.println("TO AQUI" + valuesHORARIO);
        try {
            getWritableDatabase().insert(HORARIO, null, valuesHORARIO);
            return true;
        }catch (SQLiteException ex){
            return false;
        }

    }



    public boolean validarAcessoAdmin(String usuario, String senha) {
        String sql = "Select * from ADMINISTRADOR where nome = '"+usuario+"' and senha = '"+senha+"' ";
////            //classe utilizada para ler o resultado de um registro
        Cursor cursor = getReadableDatabase().rawQuery(sql, null);

        if(cursor.getCount() > 0){
//            getWritableDatabase().execSQL("UPDATE ADMINISTRADOR SET sessaoAtiva= 'true' WHERE nome='"+usuario+"'");
            return true;
        }
        else {
            return false;
        }
    }


    public List<String> mostraAdmnis(){
        String sql = "Select * from ADMINISTRADOR";

        List<String> admins = new ArrayList<>();
        //classe utilizada para ler o resultado de um registro
        Cursor cursor = getReadableDatabase().rawQuery(sql,null);
        while (cursor.moveToNext()){
            admins.add(cursor.getString(1));
        }

        return admins;
    }


    /*************************************************************************************/

// UTILIZAR ESSE METODO PARA TESTES, CASO NECESSARIO


//    public List<String> mostraHorarios(){
//        String sql = "Select * from HORARIO";
//
//        List<String> horarios = new ArrayList<>();
//            //classe utilizada para ler o resultado de um registro
//            Cursor cursor = getReadableDatabase().rawQuery(sql,null);
//        while (cursor.moveToNext()){
//            horarios.add(cursor.getString(0));
//        }
//
//        return horarios;
//    }


//        }
//
//        public List<Aluno> listar(){
////
////            List<Aluno> alunos = new ArrayList<Aluno>();
////
////            String sql = "Select * from tb_aluno  by nome";
////            //classe utilizada para ler o resultado de um registro
////            Cursor cursor = getReadableDatabase().rawQuery(sql,null);
////
////            try{
////                while(cursor.moveToNext()){
////
////                    Aluno aluno = new Aluno();
////                    aluno.setId(cursor.getLong(0));
////                    aluno.setNome(cursor.getString(1));
////                    aluno.setDataNascimento(cursor.getString(2));
////                    aluno.setSerie(cursor.getString(3));
//                    aluno.setTelefone(cursor.getString(4));
//                    aluno.setEmail(cursor.getString(5));
//
//                    alunos.add(aluno);
//                }
//            }catch(SQLiteException e){
//
//            }finally{
//
//            }
//
//            return alunos;
//        }
//
//        public void deletarAluno(Aluno aluno){
//            String args[]={aluno.getNome().toString()};
//            getWritableDatabase().delete(TABELA,"nome=?",args);
//        }
//
//        public void alterarAluno(Aluno aluno){
//
//            String where;
//
//            where = "_id=" + aluno.getId();
//
//            ContentValues values = new ContentValues();
//
//            values.put("nome", aluno.getNome());
//            values.put("dataNascimento", aluno.getDataNascimento());
//            values.put("serie", aluno.getSerie());
//            values.put("telefone", aluno.getTelefone());
//            values.put("email", aluno.getEmail());
//
//            getWritableDatabase().update(TABELA,values,where,null);
//            getWritableDatabase().close();
//        }
//
//
//
//
////    public void alterarAluno(Aluno aluno){
////
////        ContentValues values = new ContentValues();
////
////        values.put("nome", aluno.getNome());
////        values.put("dataNascimento", aluno.getDataNascimento());
////        values.put("serie", aluno.getSerie());
////        values.put("telefone", aluno.getTelefone());
////        values.put("email", aluno.getEmail());
////
////        String args[]={aluno.getNome().toString()};
////        deletarAluno(aluno);
////        getWritableDatabase().update(TABELA, values, "nome=?", args);
////    }

//    }



}
