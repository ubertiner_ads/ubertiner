package ubertiner.mobile.desenv.ubertiner.MAPAS;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.R;

public class MapsActivity1 extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    float latitude;
    float longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent in = getIntent();
        this.latitude = Float.parseFloat(in.getStringExtra("latitude"));
        this.longitude = Float.parseFloat(in.getStringExtra("longitude"));

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng uberaba = new LatLng(this.latitude, this.longitude);
        mMap.addMarker(new MarkerOptions().position(uberaba).title("Marker in Uberaba"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(uberaba));
        CameraPosition posicao = new CameraPosition(uberaba, 15, 0, 0);
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(posicao), 300, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_mapas, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.meuLocal:
                this.localizacaoAtual();
                break;
            case R.id.tipoMapas:
                this.mudarTipoMapas();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void localizacaoAtual() {

        mMap.clear();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);
        //mMap.setOnMyLocationButtonClickListener();

        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                //limpa os marcadores do mapa
                mMap.clear();
                //pega as coordenadas da localização (latitude, longitude)
                LatLng minhaLocalizacao = new LatLng(location.getLatitude(),location.getLongitude());
                //faz uma marcação no mapa na latitude e longitude informada
                mMap.addMarker(new MarkerOptions().position(minhaLocalizacao));
                //faz a animação da aproximação da câmera
                CameraPosition minhapos = new CameraPosition(minhaLocalizacao,15,0,0);
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(minhapos), 3000, null);
            }
        });

    }

    public void mudarTipoMapas(){


        if(mMap.getMapType() == GoogleMap.MAP_TYPE_HYBRID){
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        }else if(mMap.getMapType() == GoogleMap.MAP_TYPE_TERRAIN){
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        }else if(mMap.getMapType() == GoogleMap.MAP_TYPE_SATELLITE){
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        }else{
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }


    }

//    public void buscar(View view){
//
//        List<Address> addressList = null;
//
//        EditText location_edt = (EditText) findViewById(R.id.edt_busca);
//        String location = location_edt.getText().toString();
//
//        if(location != null || !location.equals("")){
//
//            Geocoder geocoder = new Geocoder(this);
//
//            try{
//
//                addressList = geocoder.getFromLocationName(location,1);
//
//            }catch (IOException e){
//
//                e.printStackTrace();
//
//            }
//            Address address = addressList.get(0);
//
//
//
//            LatLng latLng  = new LatLng(address.getLatitude(),address.getLongitude());
//            mMap.addMarker(new MarkerOptions().position(latLng).title("Marcador"));
//
//            CameraPosition update = new CameraPosition(latLng,15,0,0);
//
//            // mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng),5000,null);
//            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(update),5000,null);
//
//        }
//    }


}
