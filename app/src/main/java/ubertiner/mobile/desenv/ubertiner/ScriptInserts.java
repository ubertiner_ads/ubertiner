package ubertiner.mobile.desenv.ubertiner;

import android.content.ContentValues;

/**
 * Created by SamuelMiranda on 05/05/2016.
 */
public class ScriptInserts {

    private ContentValues usuarios;
    private ContentValues cidades;
    private ContentValues regioes;
    private ContentValues bairros;
    private ContentValues ruas;
    private ContentValues onibus;
    private ContentValues horarios;

//---------------------------------INICIALIZA TODOS OS CONTENTVALUES... ---------------------------
    public ScriptInserts() {

        this.cidades.put("nomeCidade", "uberaba");
        this.cidades.put("estado", "minas gerais");
        this.cidades.put("nomeCidade", "uberandia");
        this.cidades.put("estado", "minas gerais");
        this.cidades.put("nomeCidade", "araxa");
        this.cidades.put("estado", "minas gerais");



        this.usuarios.put("nome", "administrador");
        this.usuarios.put("senha", "123456");
        this.usuarios.put("nomeCidade", "araxa");
        this.usuarios.put("nome", "mayco");
        this.usuarios.put("senha", "123456");
        this.usuarios.put("nomeCidade", "uberaba");
        this.usuarios.put("nome", "douglas");
        this.usuarios.put("senha", "123456");
        this.usuarios.put("nomeCidade", "uberaba");
        this.usuarios.put("nome", "samuel");
        this.usuarios.put("senha", "123456");
        this.usuarios.put("nomeCidade", "uberaba");



        this.regioes.put("nomeRegiao", "NORTE");
        this.regioes.put("nomeCidade", "uberaba");
        this.regioes.put("nomeRegiao", "SUL");
        this.regioes.put("nomeCidade", "uberaba");
        this.regioes.put("nomeRegiao", "LESTE");
        this.regioes.put("nomeCidade", "uberaba");



        this.bairros.put("nomeBairro", "Centro");
        this.bairros.put("nomeCidade", "uberaba");
        this.bairros.put("nomeBairro", "Fabricio");
        this.bairros.put("nomeCidade", "uberaba");
        this.bairros.put("nomeBairro", "Valim de Melo");
        this.bairros.put("nomeCidade", "uberaba");
        this.bairros.put("nomeBairro", "Cidade Jardim");
        this.bairros.put("nomeCidade", "uberaba");
        this.bairros.put("nomeBairro", "Chica Ferreira");
        this.bairros.put("nomeCidade", "uberaba");
        this.bairros.put("nomeBairro", "Jardim Esplanada");
        this.bairros.put("nomeCidade", "uberaba");
        this.bairros.put("nomeBairro", "Alfredo Freire");
        this.bairros.put("nomeCidade", "uberaba");
        this.bairros.put("nomeBairro", "Abadia");
        this.bairros.put("nomeCidade", "uberaba");
        this.bairros.put("nomeBairro", "Manoel Mendes");
        this.bairros.put("nomeCidade", "uberaba");


        this.ruas.put("nomeRua", "Joao Miguel Hueb");
        this.ruas.put("nomeBairro", "Cidade Jardim");
        this.ruas.put("nomeRua", "Avenida Guilherme Ferreira");
        this.ruas.put("nomeBairro", "Cidade Jardim");
        this.ruas.put("nomeRua", "Avenida Rodrigues da Cunha");
        this.ruas.put("nomeBairro", "Cidade Jardim");
        this.ruas.put("nomeRua", "Avenida Mei Mei");
        this.ruas.put("nomeBairro", "Jardim Esplanada");



        this.onibus.put("codigo", 1001);
        this.onibus.put("nomeOnibus", "Chica Ferreira / Terminal Oeste");
        this.onibus.put("empresa", "piracicabana");
        this.onibus.put("codigo", 1002);
        this.onibus.put("nomeOnibus", "Interbairros 1");
        this.onibus.put("empresa", "piracicabana");
        this.onibus.put("codigo", 1003);
        this.onibus.put("nomeOnibus", "Valim de Melo");
        this.onibus.put("empresa", "lider");



//        nomeOnibus VARCHAR(50) NOT NULL,
//        " +
//                "  nomeCidade VARCHAR(45) NOT NULL,\n" +
//                "  nomeRegiao VARCHAR(30) NOT NULL,\n" +
//                "  nomeBairro VARCHAR(30) NOT NULL,\n" +
//                "  nomeRua VARCHAR(50) NOT NULL,\n" +
//                "  tempo VARCHAR(45) NOT NULL,\n" +


        this.horarios.put("nomeCidade", "uberaba");
//        this.horarios.put("nomeRegiao", "uberaba");
        this.horarios.put("nomeBairro", "Cidade Jardim");
        this.horarios.put("nomeRua", "Joao Miguel Hueb");
        this.horarios.put("tempo", "14:00");
        this.horarios.put("nomeCidade", "uberaba");
//        this.horarios.put("nomeRegiao", "uberaba");
        this.horarios.put("nomeBairro", "Jardim Esplanada");
        this.horarios.put("nomeRua", "Avenida Mei Mei");
        this.horarios.put("tempo", "16:15");

    }

    public ContentValues getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ContentValues usuarios) {
        this.usuarios = usuarios;
    }

    public ContentValues getCidades() {
        return cidades;
    }

    public void setCidades(ContentValues cidades) {
        this.cidades = cidades;
    }

    public ContentValues getRegioes() {
        return regioes;
    }

    public void setRegioes(ContentValues regioes) {
        this.regioes = regioes;
    }

    public ContentValues getBairros() {
        return bairros;
    }

    public void setBairros(ContentValues bairros) {
        this.bairros = bairros;
    }

    public ContentValues getRuas() {
        return ruas;
    }

    public void setRuas(ContentValues ruas) {
        this.ruas = ruas;
    }

    public ContentValues getOnibus() {
        return onibus;
    }

    public void setOnibus(ContentValues onibus) {
        this.onibus = onibus;
    }

    public ContentValues getHorarios() {
        return horarios;
    }

    public void setHorarios(ContentValues horarios) {
        this.horarios = horarios;
    }
}
