package ubertiner.mobile.desenv.ubertiner;

/**
 * Created by SamuelMiranda on 20/06/2016.
 */
public class ScriptAdmin {

    private String sqlADMIN;
    private String sqlInsertADMIN;



    public ScriptAdmin() {

        this.sqlADMIN = "CREATE TABLE IF NOT EXISTS ADMINISTRADOR (\n" +
                "  _id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "  nome VARCHAR(45) NULL UNIQUE ON CONFLICT ROLLBACK,\n" +
                "  senha VARCHAR(45) NOT NULL);" ;

        this.sqlInsertADMIN = "insert INTO ADMINISTRADOR VALUES(0, 'admin','admin');";

    }

    public String getSqlADMIN() {
        return this.sqlADMIN;
    }

    public void setSqlADMIN(String sqlADMIN) {
        this.sqlADMIN = sqlADMIN;
    }

    public String getSqlInsertADMIN() {
        return this.sqlInsertADMIN;
    }

    public void setSqlInsertADMIN(String sqlInsertADMIN) {
        this.sqlInsertADMIN = sqlInsertADMIN;
    }
}
