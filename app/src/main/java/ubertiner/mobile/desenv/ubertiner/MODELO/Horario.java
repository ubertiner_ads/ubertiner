package ubertiner.mobile.desenv.ubertiner.MODELO;

import java.io.Serializable;
import java.sql.Time;

/**
 * Created by SamuelMiranda on 01/05/2016.
 */
public class Horario implements Serializable {

    private String nomeOnibus;
    private String nomeCidade;
    private String nomeRegiao;
    private String nomeBairro;
    private String nomeRua;
    private String tempo_1;
    private float latitude;
    private float longitude;



    public Horario() {
    }

    public Horario(String nomeOnibus, String nomeCidade, String nomeRegiao, String nomeBairro, String nomeRua, String tempo_1, float latitude, float longitude) {
        this.nomeOnibus = nomeOnibus;
        this.nomeCidade = nomeCidade;
        this.nomeRegiao = nomeRegiao;
        this.nomeBairro = nomeBairro;
        this.nomeRua = nomeRua;
        this.tempo_1 = tempo_1;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return  "Nome do onibus: '" + nomeOnibus + '\'' +
                ", Rua: '" + nomeRua + '\'' +
                ", Tempo: " + tempo_1;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getNomeOnibus() {
        return nomeOnibus;
    }

    public void setNomeOnibus(String nomeOnibus) {
        this.nomeOnibus = nomeOnibus;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public String getNomeRegiao() {
        return nomeRegiao;
    }

    public void setNomeRegiao(String nomeRegiao) {
        this.nomeRegiao = nomeRegiao;
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }

    public String getNomeRua() {
        return nomeRua;
    }

    public void setNomeRua(String nomeRua) {
        this.nomeRua = nomeRua;
    }

    public String getTempo_1() {
        return tempo_1;
    }

    public void setTempo_1(String tempo_1) {
        this.tempo_1 = tempo_1;
    }
}
