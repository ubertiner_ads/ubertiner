package ubertiner.mobile.desenv.ubertiner.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.INFO.OnibusRuaActivity;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;


/**
 * A simple {@link Fragment} subclass.
 */
public class RuasFragment extends Fragment {

    /*VARIAVEIS INSTANCIADAS - IMPORTANTES*/
    private ArrayAdapter<String> adapterRua;
    ListView lv_ruas;
    UbertinerDAO dao;
    private int adapterLayout = android.R.layout.simple_list_item_1;
    private List<String> nomeRuas = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_ruas, container, false);

        //esse context pega o contexto da classe, usado pra fazer a busca no banco quando chama o método
        Context ctx = container.getContext();

        //aqui eu inicializo o construtor da classe UbertinerDAO, responsavel pelas consultas no banco
        dao = new UbertinerDAO(ctx);

        //Instancio o meu ListView usando o ID
        lv_ruas = (ListView) v.findViewById(R.id.lv_ruas);

        //chamo o metodo que me retorna o List de Strings contendo os nomes dos ruas ja cadastrados no banco
        nomeRuas = dao.listarRuas();
        System.out.println(nomeRuas);

        //Aqui eu inicializo o meu ArrayAdapter que vai mostrar o conteudo da List no meu ListView: lv_ruas
        adapterRua = new ArrayAdapter<String>(getActivity().getBaseContext(), adapterLayout, nomeRuas);

        //Essa linha vincula o meu ListView ao meu ArrayAdapter, jogando as informações na View
        lv_ruas.setAdapter(adapterRua);

        lv_ruas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(getActivity().getBaseContext(), OnibusRuaActivity.class);
                String ruaSelecionado = (String) lv_ruas.getItemAtPosition(position);
                it.putExtra("Rua_Selecionada", ruaSelecionado);
                startActivity(it);
            }
        });

        //Retorno a View para o meu PagerView, ela é reconhecida através da interface do android
        return v;
    }

    public static RuasFragment newInstance(String text) {

        RuasFragment frag = new RuasFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        frag.setArguments(b);

        return frag;
    }
}