package ubertiner.mobile.desenv.ubertiner.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.INFO.OnibusActivity;
import ubertiner.mobile.desenv.ubertiner.INFO.OnibusHorarioActivity;
import ubertiner.mobile.desenv.ubertiner.MODELO.Horario;
import ubertiner.mobile.desenv.ubertiner.MODELO.Onibus;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnibusFragment extends Fragment {

    private ArrayAdapter<String> adapterOnibus;
    UbertinerDAO dao;
    private int adapterLayout = android.R.layout.simple_list_item_1;
    private List<String> nomeOnibus = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_onibus, container, false);

        //esse context pega o contexto da classe, usado pra fazer a busca no banco quando chama o método
        Context ctx = container.getContext();

        //aqui eu inicializo o construtor da classe UbertinerDAO, responsavel pelas consultas no banco
        dao = new UbertinerDAO(ctx);

        //Instancio o meu ListView usando o ID
        final ListView lv_onibus = (ListView) v.findViewById(R.id.lv_onibus);

        //chamo o metodo que me retorna o List de Strings contendo os nomes dos onibus ja cadastrados no banco
        nomeOnibus = dao.listarOnibus();

        //Aqui eu inicializo o meu ArrayAdapter que vai mostrar o conteudo da List no meu ListView: lv_onibus
        adapterOnibus = new ArrayAdapter<String>(getActivity().getBaseContext(), adapterLayout, nomeOnibus);

        //Essa linha vincula o meu ListView ao meu ArrayAdapter, jogando as informações na View
        lv_onibus.setAdapter(adapterOnibus);

        lv_onibus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(getActivity().getBaseContext(), OnibusActivity.class);
                String onibusSelecionado = (String) lv_onibus.getItemAtPosition(position);
                it.putExtra("Onibus_Selecionado", onibusSelecionado);
                startActivity(it);
            }
        });

        //Retorno a View para o meu PagerView, ela é reconhecida através da interface do android
        return v;
    }

    public static OnibusFragment newInstance(String text) {

        OnibusFragment f = new OnibusFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}

