package ubertiner.mobile.desenv.ubertiner.INFO;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.MODELO.Horario;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.TelaPrincipal;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;

public class OnibusRuaActivity extends FragmentActivity {

    List<Horario> horariosOnibus = new ArrayList<>();
    private ArrayAdapter<Horario> adapterRua;
    ListView lv_detalherua;
    ListView lv_detalherua2;
    private int adapterLayout = android.R.layout.simple_list_item_1;

    UbertinerDAO dao  = new UbertinerDAO(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_rua);
        lv_detalherua = (ListView) findViewById(R.id.lv_detalhe_rua);
        lv_detalherua2 = (ListView) findViewById(R.id.lv_detalhe_rua);

        lv_detalherua2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(OnibusRuaActivity.this, RuaHorarioActivity.class);
                Horario horarioSelecionado = (Horario) lv_detalherua2.getItemAtPosition(position);
                it.putExtra("Horario_Selecionado", horarioSelecionado);
                startActivity(it);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        String nomeRua = intent.getStringExtra("Rua_Selecionada");
        UbertinerDAO dao = new UbertinerDAO(this);
        List<Horario> horario = dao.buscaOnibusRua(nomeRua);
        dao.close();
        if(horario == null){
            Toast.makeText(this, "Nao ha dados cadastrados para " + nomeRua, Toast.LENGTH_LONG).show();
            Intent in = new Intent(OnibusRuaActivity.this, TelaPrincipal.class);
            startActivity(in);
        }else {
            this.adapterRua = new ArrayAdapter<Horario>(this, adapterLayout, horario);
            this.lv_detalherua.setAdapter(adapterRua);
        }
    }
}
