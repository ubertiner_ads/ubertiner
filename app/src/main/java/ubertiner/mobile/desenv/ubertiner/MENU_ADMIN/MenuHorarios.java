package ubertiner.mobile.desenv.ubertiner.MENU_ADMIN;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import ubertiner.mobile.desenv.ubertiner.CRUDS.AlterarHorarios;
import ubertiner.mobile.desenv.ubertiner.CRUDS.ExcluirHorarios;
import ubertiner.mobile.desenv.ubertiner.CRUDS.InserirHorarios;
import ubertiner.mobile.desenv.ubertiner.MenuAdmin;
import ubertiner.mobile.desenv.ubertiner.R;

public class MenuHorarios extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_horarios);
    }


    public void inserir(View view){
        Intent in = new Intent(MenuHorarios.this, InserirHorarios.class);
        startActivity(in);
    }

    public void alterar(View view){
        Intent in = new Intent(MenuHorarios.this, AlterarHorarios.class);
        startActivity(in);
    }

    public void excluir(View view){
        Intent in = new Intent(MenuHorarios.this, ExcluirHorarios.class);
        startActivity(in);
    }

    public void voltar(View view){
        Intent in = new Intent(MenuHorarios.this, MenuAdmin.class);
        startActivity(in);
    }
}
