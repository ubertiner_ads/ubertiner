package ubertiner.mobile.desenv.ubertiner.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.INFO.OnibusBairroActivity;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;


/**
 * A simple {@link Fragment} subclass.
 */
public class BairrosFragment extends Fragment {

    /*VARIAVEIS INSTANCIADAS - IMPORTANTES*/
        private ArrayAdapter<String> adapterBairro;
        ListView lv_bairros;
        UbertinerDAO dao;
        private int adapterLayout = android.R.layout.simple_list_item_1;
        private List<String> nomeBairros = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_bairros, container, false);

        //esse context pega o contexto da classe, usado pra fazer a busca no banco quando chama o método
        Context ctx = container.getContext();

        //aqui eu inicializo o construtor da classe UbertinerDAO, responsavel pelas consultas no banco
        dao = new UbertinerDAO(ctx);

        //Instancio o meu ListView usando o ID
        lv_bairros = (ListView) v.findViewById(R.id.lv_bairros);

        //chamo o metodo que me retorna o List de Strings contendo os nomes dos bairros ja cadastrados no banco
        nomeBairros = dao.listarBairros();
        System.out.println(nomeBairros);

        //Aqui eu inicializo o meu ArrayAdapter que vai mostrar o conteudo da List no meu ListView: lv_bairros
        adapterBairro = new ArrayAdapter<String>(getActivity().getBaseContext(), adapterLayout, nomeBairros);

        //Essa linha vincula o meu ListView ao meu ArrayAdapter, jogando as informações na View
        lv_bairros.setAdapter(adapterBairro);

        lv_bairros.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(getActivity().getBaseContext(), OnibusBairroActivity.class);
                String bairroSelecionado = (String) lv_bairros.getItemAtPosition(position);
                    it.putExtra("Bairro_Selecionado", bairroSelecionado);
                    startActivity(it);

            }
        });

        //Retorno a View para o meu PagerView, ela é reconhecida através da interface do android
        return v;
    }

    public static BairrosFragment newInstance(String text) {

        BairrosFragment frag = new BairrosFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        frag.setArguments(b);

        return frag;
    }
}

