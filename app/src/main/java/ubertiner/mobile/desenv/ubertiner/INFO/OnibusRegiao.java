package ubertiner.mobile.desenv.ubertiner.INFO;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ubertiner.mobile.desenv.ubertiner.MODELO.Horario;
import ubertiner.mobile.desenv.ubertiner.R;
import ubertiner.mobile.desenv.ubertiner.UbertinerDAO;

public class OnibusRegiao extends AppCompatActivity {
    List<Horario> horariosOnibus = new ArrayList<>();
    private ArrayAdapter<Horario> adapterRegiao;
    ListView lv_detalheregiao;
    private int adapterLayout = android.R.layout.simple_list_item_1;

    UbertinerDAO dao = new UbertinerDAO(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_regiao);
        lv_detalheregiao = (ListView) findViewById(R.id.lv_detalhe_rua);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        String nomeRegiao = intent.getStringExtra("Regiao_Selecionado");
        UbertinerDAO dao = new UbertinerDAO(this);
        List<Horario> horario = dao.buscaOnibusRegiao(nomeRegiao);
        dao.close();
        this.adapterRegiao = new ArrayAdapter<Horario>(this, adapterLayout, horario);
        this.lv_detalheregiao.setAdapter(adapterRegiao);
    }
}
