package ubertiner.mobile.desenv.ubertiner.MENU_ADMIN;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import ubertiner.mobile.desenv.ubertiner.CRUDS.AlterarBairros;
import ubertiner.mobile.desenv.ubertiner.CRUDS.ExcluirBairros;
import ubertiner.mobile.desenv.ubertiner.CRUDS.InserirBairros;
import ubertiner.mobile.desenv.ubertiner.MenuAdmin;
import ubertiner.mobile.desenv.ubertiner.R;

public class MenuBairros extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_bairros);
    }

    public void inserir(View view){
        Intent in = new Intent(MenuBairros.this, InserirBairros.class);
        startActivity(in);
    }

    public void alterar(View view){
        Intent in = new Intent(MenuBairros.this, AlterarBairros.class);
        startActivity(in);
    }

    public void excluir(View view){
        Intent in = new Intent(MenuBairros.this, ExcluirBairros.class);
        startActivity(in);
    }

    public void voltar(View view){
        Intent in = new Intent(MenuBairros.this, MenuAdmin.class);
        startActivity(in);
    }
}
